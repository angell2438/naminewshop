/** @format */
import {AppRegistry} from 'react-native';
import App from './src/bootstrap/App';

AppRegistry.registerComponent('naomi', () => App);