import React, { Component } from 'react';
import { Dimensions } from "react-native";
import {
    Scene,
    Stack,
    Router,
    Drawer,
    Lightbox,
} from 'react-native-router-flux';

import axios from 'axios';

import ReduxContainer from 'app/bootstrap/Redux';

import { apiUrl, apiKey } from 'app/config/api'

import * as routes from "app/config/sceneKeys";

import NavBar from "app/components/NavBar";
import NavBarWithoutAction from "app/components/NavBar/NavBarWithoutAction";
import NavBarBack from "app/components/NavBar/NavBarBack";
import NavBarSearchFilter from "app/components/NavBar/NavBarSearchFilter";
import NavBarSearch from "app/components/NavBar/NavBarSearch";
import LoadingLayout from "app/layouts/LoadingLayout";

import Auth from 'app/views/Auth';
import PasswordRecovery from 'app/views/Auth/PasswordRecovery';

// Redux
import Index from 'app/redux/connected/Index';
import Review from 'app/redux/connected/Review';
import Categories from 'app/redux/connected/Categories';
import SubCategories from 'app/redux/connected/SubCategories';
import Profile from 'app/redux/connected/Profile';
import ProfileEdit from 'app/redux/connected/ProfileEdit';
import Information from 'app/redux/connected/Information';
import InformationDetail from 'app/redux/connected/InformationDetail';
import Settings from 'app/redux/connected/Settings';
import Favorites from 'app/redux/connected/Favorites';
import Product from 'app/redux/connected/Product';
import Products from 'app/redux/connected/Products';
import SearchLightbox from 'app/redux/connected/SearchLightbox';
import Basket from 'app/redux/connected/Basket';
import Order from 'app/redux/connected/Order';
import MyOrders from 'app/redux/connected/MyOrders';
import Filter from 'app/redux/connected/Filter';

// END Redux

import drawer from 'app/components/Drawer';


export default class App extends Component {

    render() {
        const width = Dimensions.get('screen').width * .8;
        // For network debug
        GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

        axios.create({
            timeout: 4000
        });
        axios.defaults.baseURL = apiUrl;
        axios.defaults.headers.apiKey = apiKey;
        axios.defaults.method = 'post';

        return (
            <ReduxContainer>
                <Router sceneStyle={{ backgroundColor: "#fff" }}>

                    <Drawer key={'drawer'}
                        drawerPosion={'left'}
                        drawerWidth={width}
                        contentComponent={drawer}>
                        <Lightbox>
                            <Stack key="root">
                                <Scene
                                    key={routes.INDEX.key}
                                    title={routes.INDEX.title}
                                    component={Index}
                                    navBar={NavBarSearch}

                                />

                                <Scene
                                    key={routes.AUTH.key}
                                    title={routes.AUTH.title}
                                    component={Auth}
                                    navBar={NavBar}
                                />

                                <Scene
                                    key={routes.PASSWORD_RECOVERY.key}
                                    title={routes.PASSWORD_RECOVERY.title}
                                    component={PasswordRecovery}
                                    navBar={NavBarWithoutAction}

                                />

                                <Scene
                                    key={routes.FAVORITES.key}
                                    title={routes.FAVORITES.title}
                                    component={Favorites}
                                    navBar={NavBar}

                                />

                                <Scene
                                    key={routes.REVIEW.key}
                                    title={routes.REVIEW.title}
                                    component={Review}
                                    navBar={NavBar}

                                />

                                <Scene
                                    key={routes.CATEGORIES.key}
                                    title={routes.CATEGORIES.title}
                                    component={Categories}
                                    navBar={NavBar}
                                    initial
                                />

                                <Scene
                                    key={routes.SUB_CATEGORIES.key}
                                    component={SubCategories}
                                    navBar={NavBarBack}
                                />

                                <Scene
                                    key={routes.PROFILE.key}
                                    title={routes.PROFILE.title}
                                    component={Profile}
                                    navBar={NavBar}

                                />

                                <Scene
                                    key={routes.PROFILE_EDIT.key}
                                    title={routes.PROFILE_EDIT.title}
                                    component={ProfileEdit}
                                    navBar={NavBarWithoutAction}

                                />
                                <Scene
                                    key={routes.INFORMATION.key}
                                    title={routes.INFORMATION.title}
                                    component={Information}
                                    navBar={NavBarBack}

                                />

                                <Scene
                                    key={routes.INFORMATION_DETAIL.key}
                                    component={InformationDetail}
                                    navBar={NavBarBack}
                                />

                                <Scene
                                    key={routes.SETTINGS.key}
                                    title={routes.SETTINGS.title}
                                    component={Settings}
                                    navBar={NavBar}
                                />

                                <Scene
                                    key={routes.PRODUCTS.key}
                                    title={routes.PRODUCTS.title}
                                    component={Products}
                                    navBar={NavBarSearchFilter}
                                />

                                <Scene
                                    key={routes.PRODUCT.key}
                                    title={routes.PRODUCT.title}
                                    component={Product}
                                    navBar={NavBarBack}

                                />
                                <Scene
                                    key={routes.ORDER.key}
                                    title={routes.ORDER.title}
                                    component={Order}
                                    navBar={NavBarBack}

                                />

                                <Scene
                                    key={routes.BASKET.key}
                                    title={routes.BASKET.title}
                                    component={Basket}
                                    navBar={NavBarBack}
                                />

                                <Scene
                                    key={routes.MY_ORDERS.key}
                                    title={routes.MY_ORDERS.title}
                                    component={MyOrders}
                                    navBar={NavBar}
                                />

                                <Scene
                                    key={routes.FILTER.key}
                                    title={routes.FILTER.title}
                                    component={Filter}
                                    navBar={NavBar}
                                />

                            </Stack>
                            <Stack >
                                <Scene
                                    key={routes.SHEARCH_LIGHTBOX.key}
                                    component={SearchLightbox}
                                    hideNavBar
                                />
                            </Stack>

                            <Scene
                                key='loading'
                                modal={true}
                                component={LoadingLayout}
                            />

                        </Lightbox>
                    </Drawer>
                </Router>
            </ReduxContainer>
        );
    }
}