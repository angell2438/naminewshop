import React, {Component} from 'react';
import {
  View,
  Text
} from "react-native";
import PropTypes from "prop-types";

import * as stylesConfig from 'app/config/style';
import styles from "./styles";
import Button from "../Button";

class BasketPanel extends Component {


  constructor(props) {
    super(props);

    this.state = {
      editable: true,
      textTotal: 'Итого к оплате:',
      textCurrency: 'грн',
      total: 1,
    };

  }

  static propTypes = {
    type: PropTypes.string, // default, numeric, email-address, phone-pad
    name: PropTypes.string,
    total: PropTypes.number

  };

  render() {
    const {props, state} = this;
    const style = styles({...props, ...state});

    return (
      <View style={style.checkoutContainer}>
        <View style={style.content}>
          <Text style={style.text}>
            {state.textTotal}
          </Text>
          <Text style={style.text}>
            {props.totalPrice} {state.textCurrency}
          </Text>
        </View>
        <Button text='Оформить заказ'/>
      </View>
    );
  }


}

export default BasketPanel;