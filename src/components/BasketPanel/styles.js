import {StyleSheet} from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    wrapper: {
      flexDirection: 'column',
      paddingLeft: 0,
      paddingRight: 0,
      alignItems: 'center',
      alignSelf: 'center',
    },

    checkoutContainer: {
      flexDirection: 'column',
      paddingHorizontal: 15,
      paddingVertical: 15,
      backgroundColor: '#f8f8f8'
    },
    content: {
      flexDirection: 'row',
      marginBottom: 10,
      justifyContent: 'space-between'
    },
    text: {
      fontSize: 18,
      fontWeight: 'bold'
    },

  });