import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

class ButtonSocial extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    text: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    // styling
    isPrimary: PropTypes.bool,
    bgGrey: PropTypes.bool,
    colorGrey: PropTypes.bool
  }

  render() {
    const { props } = this;
    const style = styles(props);

    return (
      <TouchableOpacity
        onPress={props.onPress}
        style={style.btn}
        activeOpacity={.8}
      >        

        <Text style={style.text}>
          {props.text}
        </Text>

      </TouchableOpacity>
    )
  }

}


export default ButtonSocial;