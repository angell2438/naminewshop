import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';

export default props =>
  StyleSheet.create({
    btn: StyleSheet.flatten([
      {
        flexDirection: 'row',
        alignItems: 'center',
        padding: styles.MARGIN/2
      },
      props.style && {
        ...props.style 
      },
    ]),

    text: StyleSheet.flatten([
      {
        fontSize: styles.GUTTER + 2,
        fontWeight: '500'
      },
      props.type == 'google' && {
        color: styles.COLOR_RED
      },

      props.type == 'facebook' && {
        color: styles.COLOR_BLUE
      }
    ]),
  });

