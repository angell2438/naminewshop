import React, { Component } from 'react';
import {
  View,
  Text,
  Animated,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';

import Icon from 'app/components/Icon';
import TitleOfList from 'app/components/TitleOfList';
import CategoryItem from 'app/components/CategoryItem';


import * as stylesConfig from 'app/config/style';
import styles from './styles';

export default class CategoryChecked extends Component {


  static propTypes = {
    list: PropTypes.array,
    title: PropTypes.string,
    type: PropTypes.string,
    disableBottomBorder: PropTypes.bool,
    callback: PropTypes.func
  }

  static defaultProps = {
    list: [],
    type: 'default',
    disableBottomBorder: false
  }

  constructor() {
    super();

    this.state = {
      showAll: false,
      oneItemHeight: stylesConfig.ITEM_HEIGHT,
      animation: new Animated.Value(stylesConfig.ITEM_HEIGHT)
    }
  }

  items = () => {
    const { props } = this;
    return (
      props.list.map((item, i) => {
        return (
          <CategoryItem
            key={item.value}
            item={item}
            type='check'
            disableBottomBorder={props.disableBottomBorder && i == props.list.length - 1 ? true : false}
            onPress={() => this.onPress(item)}
          />
        )
      })
    )
  }


  render() {
    const { props, state } = this;
    const style = styles({ ...props });

    return (
      <View>


        {props.title &&
          <TitleOfList
            title={props.title}
            type={props.type}
            showRightIcon={true}
            onPress={this.toggleSelect}
          />
        }

        {props.type == 'select' &&
          <Animated.View
            style={{
              overflow: 'hidden',
              height: state.animation
            }}>
            {this.items()}
          </Animated.View>
        }

        {props.type == 'default' &&
          this.items()
        }


        {props.type == 'select' &&
          <TouchableOpacity
            onPress={this.toggleSelect}
            style={style.showAll}
            activeOpacity={.8}
          >

            <Text style={style.showAllText}>
              {state.showAll ? 'Свернуть' : 'Еще'}
            </Text>
            <Icon
              name='arrow_left'
              color={stylesConfig.COLOR_GREY}
            />

          </TouchableOpacity>
        }
      </View>
    )
  }

  onPress = (selected) => {
    const { props } = this;

    props.list.map(item => {
      if (item.value == selected.value) {
        item.checked = true
      } else {
        item.checked = false
      }
    })

    if (props.callback) {
      props.callback(props.list)
    }
  }

  toggleSelect = () => {
    const { state, props } = this;
    const allItemsHeight = state.oneItemHeight * props.list.length;

    Animated.spring(
      state.animation,
      {
        toValue: state.showAll ? state.oneItemHeight : allItemsHeight
      }
    ).start();

    this.setState({
      showAll: !state.showAll
    })
  }
}