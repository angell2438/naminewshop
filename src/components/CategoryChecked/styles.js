import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        paddingHorizontal: styles.GUTTER,
        paddingVertical: styles.GUTTER-5,
        backgroundColor: styles.COLOR_GREY_LIGHT,
        borderBottomWidth: 1,
        borderColor: styles.COLOR_GREY_MIDDLE
      },
      props.style && {
        ...props.style
      },

      props.marginTop && {
        marginTop: 20
      },
    ]),


    title: StyleSheet.flatten([
      {
        // paddingHorizontal: styles.GUTTER,
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE,
        textAlign: 'left',
      }
    ]),

    showAll: StyleSheet.flatten([
      {
        paddingHorizontal: styles.GUTTER,
        height: styles.ITEM_HEIGHT,
        flexDirection: 'row',
        alignItems: 'center'
      }
    ]),
    showAllText: StyleSheet.flatten([
      {
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE,
        paddingRight: styles.GUTTER-10
      }
    ]),
  });

