import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import AutoHeightImage from 'react-native-auto-height-image';

import Icon from 'app/components/Icon';

import * as stylesConfig from 'app/config/style';

import styles from './styles';

export default class CategoryItem extends Component {


  static propTypes = {
    item: PropTypes.object.isRequired,
    active: PropTypes.bool,
    disableBottomBorder: PropTypes.bool
  }

  state = {
    iconRight: false,
    image: true,
  }

  componentWillMount() {
    this.checkType();
  }

  render() {
    const { props, state } = this;
    const { item } = props;
    const imageWidth = 32;
    const style = styles({ ...props, imageWidth });

    let colorIcon = stylesConfig.COLOR_GREY;

    if (props.type == 'check' && item.checked) {
      colorIcon = stylesConfig.COLOR_RED
    } else {
      colorIcon = 'transparent'
    }

    return (

      <TouchableOpacity
        style={style.itemContainer}
        onPress={this.onPress}
        activeOpacity={.8}
      >
        {state.image &&
          <View style={style.iconContainer}>
            <AutoHeightImage
              width={imageWidth}
              source={{ uri: item.image }}
            />
          </View>
        }
        <View style={style.textContainer}>
          <Text style={style.text}>
            {item.name || item.title}
          </Text>
          {state.iconRight &&
            <Icon
              name='arrow_left'
              color={colorIcon}
            />
          }
        </View>
      </TouchableOpacity>

    )
  }

  componentWillReceiveProps(nextProps) {
    this.checkType();
  }

  onPress = () => {
    const { data, onPress } = this.props;

    // if (!scene) {
    //   alert('WoW, set scene please')
    //   return false
    // }

    // Actions[routes[scene].key].call();

    if (onPress) {
      onPress();
    }
  }

  checkType = () => {
    const { props } = this;

    switch (props.type) {
      case 'subcategory':
        this.setState({
          iconRight: true,
          image: false
        })
        break;

      case 'check':
        this.setState({
          iconRight: true,
          image: false
        })
        break;
    }

  }
}