import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    itemContainer: StyleSheet.flatten([
      {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: styles.GUTTER,
      },
      props.fullWidth && {
        paddingLeft: 0
      },
      props.active && {
        backgroundColor: styles.COLOR_GREY_LIGHT
      },
    ]),

    iconContainer: StyleSheet.flatten([
      {
        flexBasis: 32 + 10,
        paddingRight: 10,
        flexShrink: 0,
      }
    ]),

    textContainer: StyleSheet.flatten([
      {
        height: styles.ITEM_HEIGHT,
        flex: 1,
        flexBasis: '100%',
        flexShrink: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingRight: styles.GUTTER,
        ...stylesGlobal.borderBottom
      },
      props.fullWidth && {
        paddingLeft: styles.GUTTER
      },
      props.disableBottomBorder && {
        borderBottomWidth: 0
      }
    ]),

    text: StyleSheet.flatten([
      {
        // paddingHorizontal: styles.GUTTER,
        color: styles.COLOR_GREY,
        fontSize: styles.FONT_SIZE,
        textAlign: 'left',
      },
      props.item.checked && {
        color: styles.COLOR_RED
      },
    ])
  });

