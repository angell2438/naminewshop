import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";

// import Svg, { Path } from "react-native-svg";

import styles from "./styles";

class Checkbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: props.isChecked === undefined ? false : true
    };
  }

  static propTypes = {
    isChecked: PropTypes.bool,
    hasToggleText: PropTypes.bool,
    onPress: PropTypes.func,
    text: PropTypes.string,

    // styling
    textToLeft: PropTypes.bool
  };

  render() {
    const { props, state } = this;
    const style = styles({ ...props, ...state });

    return (
      <TouchableOpacity
        onPress={this.handlePress}
        style={style.container}
        activeOpacity={0.6}
      >
        {props.textToLeft && this.getCheckboxText(props.text, style)}

        {this.getCheckboxMark(style)}

        {!props.textToLeft && this.getCheckboxText(props.text, style)}
      </TouchableOpacity>
    );
  }

  getCheckboxText(text, style) {
    return (
      <View style={style.label_container}>
        <Text style={style.label_text} numberOfLines={1}>
          {text}
        </Text>
      </View>
    );
  }

  getCheckboxMark(style) {
    return this.props.hasToggleText ? (
      <Text style={style.label_mark}>{this.getTextMark()}</Text>
    ) : (
        <View style={style.mark}>
          <View style={style.mark__check}>
            {/* <Svg width="14" height="11" viewBox="0 0 14 11">
              <Path
                fill="#fff"
                d="m5.049505,11.049505l-5,-5.3l1.4,-1.5l3.6,3.8l7.6,-8l1.4,1.5l-9,9.5z"
              />
            </Svg> */}
          </View>
        </View>
      );
  }

  getTextMark() {
    return this.state.isChecked ? "ON" : "OFF";
  }

  handlePress = () => {
    const { props } = this;
    this.setState({
      isChecked: !this.state.isChecked
    });

    if (typeof props.onPress === "function") props.onPress();
  };
}

export default Checkbox;
