import { StyleSheet } from "react-native";
import * as styles from 'app/config/style';


export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        flex: 1,
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "flex-start",
        alignItems: "center",
        paddingVertical: styles.GUTTER/2,
      },
      props.style && {
        ...props.style 
      }
    ]),

    mark: StyleSheet.flatten([
      {
        width: styles.GUTTER,
        height: styles.GUTTER,
        borderWidth: 1,
        borderRadius: 50,
        borderColor: styles.COLOR_GREY_LIGHTER,
        position: "relative"
      },
      props.isChecked && {
        backgroundColor: styles.COLOR_RED,
        borderColor: styles.COLOR_RED
      }
    ]),

    mark__check: StyleSheet.flatten([
      {
        position: "absolute",
        top: 2,
        right: -2,
        opacity: 0
      },
      props.isChecked && {
        opacity: 1
      }
    ]),

    label_mark: StyleSheet.flatten([
      {
        fontSize: 12,
        fontWeight: "400",
        color: styles.COLOR_GREY
      },
      props.isChecked && {
        color: styles.COLOR_GREY
      }
    ]),

    label_container: StyleSheet.flatten([
      {
        marginLeft: styles.GUTTER/2,
        flex:1
      },
      props.textToLeft && {
        marginRight: 18
      }
    ]),

    label_text: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        fontWeight: "400",
        color: styles.COLOR_BLACK
      }
    ])
  });
