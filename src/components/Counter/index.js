import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Text, TouchableOpacity, View} from "react-native";
import PropTypes from "prop-types";
import {
    changeQuantity
} from 'app/redux/actions/basket';
import styles from "./styles";

class Counter extends Component {


    constructor(props) {
        super(props);

        this.state = {
            editable: true,
            initialValue: this.props.quantity,
        };
        this.counter = this.props.quantity;
    }



    static propTypes = {
        type: PropTypes.string, // default, numeric, email-address, phone-pad
        name: PropTypes.string,
        initialValue: PropTypes.number

    };

    componentWillMount(){

    }

    render() {
        const {props, state} = this;
        const style = styles({...props, ...state});

        return (
            <View style={{flexDirection: 'row'}}>
                <View style={ style.vertical }>
                    <TouchableOpacity
                        style={style.button}
                        onPress={this._decrement}
                    >
                        <Text style={ style.buttonText }>-</Text>
                    </TouchableOpacity>
                    <Text
                        style={ style.textInput }
                    >
                        { props.quantity }
                    </Text>
                    <TouchableOpacity
                        style={ style.button }
                        onPress={this._increment}
                    >
                        <Text style={ style.buttonText }>+</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    _increment = () => {
        const {state} = this;
        this.counter++;
        this.props.changeQuantity({quantity: this.counter, id: this.props.currentId});
    }

    _decrement = () => {
        const {state} = this;

        if (this.counter > 1) {
            this.counter--;
            this.props.changeQuantity({quantity: this.counter, id: this.props.currentId});
        }
    }


}


export default
connect(null, {changeQuantity})(Counter);
