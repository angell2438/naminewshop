import {StyleSheet} from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
    StyleSheet.create({
        wrapper: {
            flex: 1,
            backgroundColor: '#eeeeee'
        },

        vertical: {
            flexDirection: 'row',
            paddingLeft: 0,
            paddingRight: 0,
            alignItems: 'center',
            alignSelf: 'center',

        },

        horizontal: {
            flexDirection: 'row'
        },

        textInput: {
            backgroundColor: 'transparent',
            textAlign: 'center',
            width: 40,
            height: 25,
            color: '#000',
            borderWidth: 0,
            borderColor: 'transparent',
            fontSize: 14,
            lineHeight: 25,
            paddingHorizontal: 5
        },

        button: {
            width: 25,
            height: 25,
            padding: 0,
            borderRadius: 20,
            borderWidth: 1,
            backgroundColor: 'transparent',
            alignItems: 'center',
            textAlign: 'center',

        },

        image: {
            width: 18,
            height: 18
        },

        buttonText: {
            fontSize: 25,
            lineHeight: 25,
        }
    });