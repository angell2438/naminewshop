import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import { Actions } from "react-native-router-flux";
import PropTypes from 'prop-types';

import * as routes from "app/config/sceneKeys";

import NumberBadge from 'app/components/NumberBadge';
import Icon from 'app/components/Icon';
import styles from './styles';

class DrawerItem extends Component {

  static propTypes = {
    item: PropTypes.object.isRequired,
    active: PropTypes.bool
  }

  onPress = () => {
    const { onPress, scene } = this.props.item;

    if (!scene) {
      alert('WoW, set scene please')
      return false
    }

    Actions[routes[scene].key].call();

    if (onPress) {
      onPress();
    }
  }

  render() {
    const { props } = this;
    const style = styles({ ...props.item });


    return (

      <TouchableOpacity
        style={style.itemContainer}
        onPress={this.onPress}
        activeOpacity={.8}
      >
        <View style={style.textContainer}>
          <Icon
            name={props.item.icon}
            style={style.icon}
          />
          <Text style={style.text}>
            {props.item.title}
          </Text>
        </View>
        {props.item.newCount &&
          <NumberBadge
            number={props.item.newCount}
          />
        }
      </TouchableOpacity>

    )
  }
}


export default DrawerItem;