import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';

export default props =>
  StyleSheet.create({
    itemContainer: StyleSheet.flatten([
      {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: styles.GUTTER
      },
      props.active && {
        backgroundColor: styles.COLOR_GREY_LIGHT
      },
    ]),

    textContainer: StyleSheet.flatten([
      {
        flexDirection: 'row',
        alignItems: 'center',
      }
    ]),

    icon: StyleSheet.flatten([
      {
        color: styles.COLOR_RED,
        fontSize: 21,
      },

    ]),

    text: StyleSheet.flatten([
      {
        paddingHorizontal: styles.GUTTER,
        color: styles.COLOR_GREY,
        textAlign: 'left',
      }
    ])
  });

