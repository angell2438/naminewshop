import React, { Component } from 'react';
import {
  View,
  Image,
  ScrollView,
  StatusBar,
  Platform
} from 'react-native';
import drawerData from 'app/config/drawerData';
import DrawerItem from './DrawerItem';

import styles from './styles'

class Drawer extends Component {

  render() {
    const style = styles({
      'statusBarHeight': Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
      'heightLogoContainer': 60,
    });
    return (
      <View style={style.container}>

        <View style={style.logoContainer}>
          <Image style={style.logo} source={require('app/img/Artboard.png')} />
        </View>
        <View style={style.scrollContainer}>
          <ScrollView bounces={false}>
            {drawerData.map(section =>
              <View key={section.id} style={style.menuSection}>
                {section.data.map(item =>
                  <DrawerItem
                    key={item.id}
                    item={item}
                  />
                )}
                <View style={style.line} />
              </View>
            )}
          </ScrollView>
        </View>

      </View>
    )
  }
}


export default Drawer;