import { Dimensions, StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        paddingTop: props.statusBarHeight

      },

    ]),

    logoContainer: StyleSheet.flatten([
      {
        flexShrink: 0,
        paddingHorizontal: styles.GUTTER,
        justifyContent: 'center',
        alignItems: 'center',
        height: props.heightLogoContainer,
        ...stylesGlobal.borderBottom
      }
    ]),

    menuSection: StyleSheet.flatten([
      {

      }
    ]),

    line: StyleSheet.flatten([
      {
        marginLeft: styles.GUTTER,
        ...stylesGlobal.borderBottom
      }
    ]),

    logo: StyleSheet.flatten([
      {
        height: 20
      }
    ]),

    scrollContainer: StyleSheet.flatten([
      {
        flexShrink: 0,
        height: Dimensions.get('window').height - props.heightLogoContainer + props.statusBarHeight
      }
    ])
  });

