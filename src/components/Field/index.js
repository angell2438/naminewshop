import React, { Component } from "react";
import {
  TextInput,
    Keyboard,
    Text,
  View
} from "react-native";
import PropTypes from "prop-types";

import Icon from 'app/components/Icon';
import * as stylesConfig from 'app/config/style';
import * as Animatable from 'react-native-animatable';
import styles from "./styles";

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value !== undefined ? props.value : "",
      hasFocus: false,
      hasValue: false,
      countSymbol: 0,
      placeholderColor: stylesConfig.COLOR_GREY,
      iconColor: stylesConfig.COLOR_RED
    };

  }

  static propTypes = {
    type: PropTypes.string, // default, numeric, email-address, phone-pad
    name: PropTypes.string,
    callback: PropTypes.func,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    icon: PropTypes.string,
    styleType: PropTypes.string,
    unfocus: PropTypes.bool,
    letterLimit: PropTypes.number,
    // styling
    inputStyle: PropTypes.string,
    hasFocus: PropTypes.bool,
    hasValue: PropTypes.bool,
		animatedFocus: PropTypes.bool,
  };

  static defaultProps = {
    type: "default",
    unfocus: false,
  };

  componentWillMount() {
    const { props } = this;

    switch (props.styleType) {
      case "search":
        this.setState({
          placeholderColor: stylesConfig.COLOR_GREY,
          iconColor: stylesConfig.COLOR_GREY
        })
        break;
    }
  }

  componentWillReceiveProps(nextProps){

    if(nextProps.unfocus){
        this.handleBlur()
    }
  }

  render() {
    const { props, state } = this;
    const style = styles({ ...props, ...state });
    const propsInput = {};
    if(props.letterLimit){
      propsInput.maxLength = props.letterLimit;
    }
    return (
      <View style={style.wrap}>
        <View style={style.container}>
          {props.icon &&
            <View  style={[style.icon, { transform: [{translateX: props.animatedFocus ? 70 : 0}] }  ]}>
              <Icon
                name={props.icon}
                color={state.iconColor}
              />
            </View>
          }
          <TextInput
            value={state.value}
            multiline={false}
            underlineColorAndroid="transparent"
            keyboardType={props.type}
            placeholder={props.placeholder}
            placeholderTextColor={state.placeholderColor}
            secureTextEntry={props.secureTextEntry}
            onChangeText={this.handleChangeText}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            style={[style.input, props.animatedFocus &&  state.hasFocus && {paddingLeft: 100}, props.customStyle && {...props.customStyle} ]}
            selectionColor={stylesConfig.COLOR_RED}
            autoFocus={this.props.autoFocus}
            ref={this.props.innerRef}
          />
        </View>
        {props.letterLimit &&
        <Text style={style.count}>
          {state.countSymbol}
          /
          {props.letterLimit}
        </Text>
        }
      </View>
    );
  }

  handleFocus = () => {
    this.setState({
      hasFocus: true
    });

    if (typeof this.props.onFocus === "function") this.props.onFocus();
  };

  handleBlur = () => {
    const { onBlur } = this.props;

    this.setState({
      hasFocus: false
    });

      Keyboard.dismiss()
    if (typeof onBlur === "function") onBlur();
  };

  handleChangeText = value => {
    const { name, letterLimit, callback } = this.props;


      this.setState({
        countSymbol: value.length,
        hasValue: value.length ? true : false,
        value: value
      });

    if (typeof callback === "function")
      callback({
        name: name,
        value: value
      });
  };
}

export default Input;
