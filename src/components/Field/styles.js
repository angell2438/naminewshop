import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    wrap: StyleSheet.flatten([
      {
        paddingVertical: styles.MARGIN / 2,
        // borderColor: 'rgba(255,255,255, .3)'
      },
      props.style &&{
        ...props.style
      }
    ]),

    container: StyleSheet.flatten([
      {
        flexDirection: 'row',
        alignItems: 'center',
        // borderColor: 'rgba(255,255,255, .3)'
      },
      props.styleType == 'search' && {
        backgroundColor: styles.COLOR_GREY_DARK,
        borderRadius: 5
      }
      // props.label && props.inputStyle == "clear" && {
      //   paddingTop: 18,
      // },
    ]),



    input: StyleSheet.flatten([
      {
        ...stylesGlobal.borderBottom,
        height: styles.FIELD_HEIGHT,
        alignSelf: 'stretch',
        alignItems: 'center',
        fontSize: 14,
        // paddingVertical: styles.GUTTER / 2,
        fontWeight: '400',
        color: styles.COLOR_GREY,
        flexBasis: 'auto',
        flexShrink: 2,
        width: '100%',
      },

      props.styleType == 'search' && {
        borderBottomWidth: 0,
        height: styles.FIELD_HEIGHT
      }
    ]),

    icon: StyleSheet.flatten([
      {
        fontSize: 21,
        flexShrink: 0,
        marginRight: styles.MARGIN / 2,
        borderRadius: 5
      },
    ]),

    count: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE-4,
        color: styles.COLOR_GREY,
        paddingTop:styles.GUTTER - 10,
        textAlign: 'right'
      },
    ]),


  });
