import React, { Component } from 'react';
import {
  View,
  Text
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

export default class FieldStatic extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    title: PropTypes.string.isRequired,
    text: PropTypes.string,

    // styling
    style: PropTypes.object,
  }

  render() {
    const { props } = this;
    const style = styles(props);

    return (
      <View
        style={style.container}
      >

        <Text
          style={style.title}
        >
          {props.title}
        </Text>

        <Text
          style={style.text}
        >
          {props.text}
        </Text>

      </View>
    )
  }

  onPress = () => {
    const { props } = this;

    if (props.onPress) {
      props.onPress()
    }
  }
}
