import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginBottom: styles.GUTTER + 5,
      },
      props.style && {
        ...props.style
      },

    ]),

    title: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE - 2,
        color: styles.COLOR_GREY,
        flexBasis: '100%',
        flexShrink: 0,
      }
    ]),

    text: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        color: styles.COLOR_BLACK,
        paddingTop: styles.GUTTER - 5,
        flexBasis: '100%',
        flexShrink: 0,
      }
    ]),
  });

