import React, { Component } from "react";
import {
  TouchableOpacity,
  Text
} from "react-native";
import PropTypes from "prop-types";

import Icon from 'app/components/Icon';
import * as stylesConfig from 'app/config/style';
import styles from "./styles";

export default class FilterButton extends Component {
  constructor(props) {
    super(props);



  }

  static propTypes = {
    icon: PropTypes.string,
    text: PropTypes.string,
    borderLeft: PropTypes.bool,
  };

  static defaultProps = {
    borderLeft: false,
  };


  render() {
    const { props, state } = this;
    const style = styles({ ...props, ...state });

    return (
      <TouchableOpacity
        onPress={props.onPress}
        style={style.btn}
        activeOpacity={.6}
      >

        <Icon
          name={props.icon}
          color={stylesConfig.COLOR_RED}
        />
        {props.text &&
          <Text style={style.text}>
            {props.text}
          </Text>
        }

      </TouchableOpacity>
    );
  }


}