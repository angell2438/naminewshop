import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    btn: StyleSheet.flatten([
      {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        height: styles.GUTTER * 3,
        paddingRight: styles.GUTTER,
        paddingLeft: styles.GUTTER-10,
        flexGrow: 1
      },

      props.style && {
        ...props.style
      },

      !props.text && {
        flexBasis: styles.GUTTER * 3,
        justifyContent: 'center',
        paddingRight: 0,
        paddingLeft: 0,
        flexGrow: 0
      },
      
      props.borderLeft && {
        borderLeftWidth: 1,
        borderColor: styles.COLOR_GREY_MIDDLE
      }

    ]),

    text: StyleSheet.flatten([
      {

        fontSize: styles.FONT_SIZE - 2,
        color: styles.COLOR_DARK
      },

    ]),
  });
