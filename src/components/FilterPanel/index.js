import React, { Component } from "react";
import {
  // ModalPanel,
  View,
} from "react-native";
import PropTypes from "prop-types";

import FilterButton from 'app/components/FilterButton';
import styles from "./styles";

import connect from "react-redux/es/connect/connect";
import { getSorted } from 'app/redux/actions/products';
import Button from "app/components/Button";
import * as Animatable from 'react-native-animatable';

import { PRODUCTS } from 'app/redux/types';

export default class FilterPanel extends Component {


  constructor (props) {
    super(props);

    this.state = {
      modalVisible: false,
    };

  }

  static propTypes = {
    type: PropTypes.string,
    onSortRating: PropTypes.func,
    onSortPopular: PropTypes.func,
    visible: PropTypes.bool,
    modalVisible: PropTypes.bool
    // default, numeric, email-address, phone-pad

  };

  static defaultProps = {
    type: "default",
    modalVisible: true,
  };


  render() {
    const { props, state } = this;
    const style = styles({ ...props, ...state });
    return (
      <View style={style.wrap}>

        <FilterButton
          icon="sort"
          text="Сортировка"
          onPress={this.openModal}
        />

        <FilterButton
          icon="filter"
          text="Фильтр"
          borderLeft={true}
          onPress={this.sort}
        />

        <FilterButton
          icon="arrow_left"
          borderLeft={true}
          onPress={this.changeViewProducts}
        />
        <FilterButton
          icon="share"
          borderLeft={true}
          onPress={this.sort}
        />

        <Modal
          animationType={'fade'}
          transparent={true}
          visible={state.modalVisible}
          onRequestClose={() => {
            this.setState({ modalVisible: false })
          }}>
          <View style={style.ContainerSortList}>
            <Animatable.View
              animation={'fadeInUpBig'}
              useNativeDriver
              ref={ref => this.filter = ref}
              delay={0}
              duration={400}
              style={style.SortListWrapper}>
              <View style={style.SortList}>
                <Button
                  style={style.SortListBtn}
                  text='По рейтенгу'
                  onPress={() => this.filterToggle(PRODUCTS.sortedRaring)}
                  isBlue={true}
                />
                <Button
                  style={style.SortListBtn}
                  isBlue={true}
                  onPress={() => this.filterToggle(PRODUCTS.sortedPopular)}
                  text='По популярности'
                />
              </View>
              <Button
                style={style.SortListBtnClose}
                onPress={() => {
                  this.filterToggle()
                }}
                isBlue={true}
                text='Отмена'
              />
            </Animatable.View>
          </View>
        </Modal>
      </View>

    );
  }

  openModal = () => {
    this.setState({ modalVisible: true });

  }

  filterToggle(type = '') {
    const { props } = this;
    this.filter.fadeOutDownBig(400).then(() => {
      this.setState({
        modalVisible: false
      }, () => {
        // debugger
        props.getSorted(type)
      })
    })
  }
  changeViewProducts = () => {
    // todo: зробити поки хардкодом вигляд продуктів і потім вже через логіку редаксу настроїти
    console.log('changeViewProducts')
  }
}

function mapStateToProps(state) {
  return {
  }
}

export default connect(mapStateToProps, { getSorted })(FilterPanel)
