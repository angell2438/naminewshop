import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

class NumberBadge extends Component {

  static propTypes = {
    number: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    style: PropTypes.object,
    blue: PropTypes.bool,
    center: PropTypes.bool
  }

  render() {
    const { props } = this;
    const style = styles(props);

    return (

      <TouchableOpacity
        onPress={props.onPress}
        style={[style.container, props.style]}
        activeOpacity={.8}
      >

        <Text style={style.text}>
          {props.text}
        </Text>

      </TouchableOpacity>

    )
  }
}


export default NumberBadge;