import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {

      },
      props.style && {
        ...props.style
      }
    ]),

    text: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        color: styles.COLOR_RED,
        fontWeight: '500',
        paddingVertical: styles.GUTTER / 2
      },
      props.blue && {
        color: styles.COLOR_BLUE_LIGHT,
      },
      props.center && {
        textAlign: 'center',
      }
    ]),
  });

