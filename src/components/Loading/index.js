import React, { Component } from 'react';
import {
  Modal,
  View,
  Text
} from 'react-native';

import PropTypes from "prop-types";

import styles from './styles';

export default class Loading extends Component {

  static propTypes = {
    show: PropTypes.bool,
    afterClose: PropTypes.func,
  };

  static defaultProps = {
    show: false
  };

  render() {
    const { props, state } = this;
    const style = styles(props);

    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={props.show}
        onRequestClose={() => {
          this.afterClose();
        }}
      >
        <View
          style={[
            style.container
          ]}
        >
          <View style={style.overlay} />

          <Text style={style.text}>
            Загрузка...
          </Text>

        </View>
      </Modal>
    )
  }

  show = () => {
    const { props, state } = this;

    // Animated.timing(state.opacity, {
    //   duration: 150,
    //   toValue: 1
    // }).start()

  }

  close = () => {
    const { props } = this;

    if (props.afterClose == 'function') {
      props.afterClose()
    }
  }
}