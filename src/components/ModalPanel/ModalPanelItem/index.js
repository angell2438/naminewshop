import React, { Component } from 'react';

import Button from "app/components/Button";
import styles from "./styles";

import { PRODUCTS } from 'app/redux/types';
import { Actions } from "react-native-router-flux";
import PropTypes from "prop-types";

class ModalPanelItem extends Component {

  static propTypes = {
    item: PropTypes.object.isRequired,
    active: PropTypes.bool
  }

  render () {
    const {props, state} = this;
    const style = styles({...props, ...state});
    return (
      <Button
        style={style.SortListBtn}
        isBlue={true}
        onPress={props.onClick}
        text={props.item.title}
      />
    );
  }


  // filterToggle (type = '') {
  //   const { props } = this;
  //   this.filter.fadeOutDownBig(400).then(() => {
  //     this.setState({
  //       modalVisible: false
  //     }, () => {
  //       // debugger
  //       props.getSorted(type)
  //     })
  //   })
  // }

}
export default ModalPanelItem;
