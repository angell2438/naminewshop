import {StyleSheet} from 'react-native';

export default props =>
  StyleSheet.create({
    SortListBtn: {
      height: 60,
      width: '100%',
      borderWidth: 0,
      borderBottomWidth: 1,
      borderColor: '#ccc',
      color: '#3a96ff',
      fontSize: 14,
      backgroundColor: '#fff',
      marginBottom: 0,
    },
    SortListBtnClose: {
      height: 60,
      width: '100%',
      borderWidth: 0,
      borderBottomWidth: 0,
      borderColor: '#ccc',
      color: '#3a96ff',
      fontSize: 14,
      backgroundColor: '#fff',
      marginBottom: 0,
      borderRadius: 14,
    },
  });
