import React, { Component } from 'react';
import {
  Modal, ScrollView,
  View
} from 'react-native';

import Button from "app/components/Button";
import * as Animatable from 'react-native-animatable';
import styles from "./styles";

import { PRODUCTS } from 'app/redux/types';
import { connect } from "react-redux";
import { getSorted } from 'app/redux/actions/products';
import modalPanel from 'app/config/modalPanel';
import ModalPanelItem from "./ModalPanelItem";

class ModalPanel extends Component {

  constructor (props) {
    super(props);

  }

  componentWillReceiveProps (nextProps) {

    if (nextProps.modalVisible) {
      // this.handleBlur()
      console.log('nextProps', nextProps)
    }
  }

  render () {
    const { props, state } = this;
    const style = styles({ ...props, ...state });
    return (
      <Modal
        animationType={'fade'}
        transparent={true}
        visible={props.modalVisible}
        onRequestClose={() => {
          this.setState({ modalVisible: false })
        }}>
        <View style={style.ContainerSortList}>
          <Animatable.View
            animation={'fadeInUpBig'}
            useNativeDriver
            ref={ref => this.filter = ref}
            delay={0}
            duration={400}
            style={style.SortListWrapper}>
              <View style={style.SortList}>
                {modalPanel['modalSort'].data.map(item =>
                  <ModalPanelItem
                    key={item.id}
                    item={item}
                    onClick={()=> this.filterToggle(item.type)}
                  />
                )}
              </View>
            <Button
              style={style.SortListBtnClose}
              onPress={() => {
                this.filterToggle()
              }}
              isBlue={true}
              text='Отмена'
            />
          </Animatable.View>
        </View>
      </Modal>
    );
  }

  filterToggle (type = '') {
    const { props } = this;
    this.filter.fadeOutDownBig(400).then(() => {
      props.onClose(false)
      type.length && props.getSorted(type)
    })
  }

}

function mapStateToProps (state) {
  return {}
}

export default connect(mapStateToProps, { getSorted })(ModalPanel)