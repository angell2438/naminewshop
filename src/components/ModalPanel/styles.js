import {StyleSheet} from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    ContainerSortList: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.75)'

    },
    SortListWrapper: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 'auto',
      width: '90%',
      borderRadius: 14,
      borderWidth: 0,
      position: 'absolute',
      bottom: 14,
      overflow: 'hidden',
    },

    SortList: {
      backgroundColor: "#fff",
      marginBottom: 14,
      width: '100%',
      borderRadius: 14,
      overflow: 'hidden',
    },

    SortListBtn: {
      height: 60,
      width: '100%',
      borderWidth: 0,
      borderBottomWidth: 1,
      borderColor: '#ccc',
      color: '#3a96ff',
      fontSize: 14,
      backgroundColor: '#fff',
      marginBottom: 0,
    },
    SortListBtnClose: {
      height: 60,
      width: '100%',
      borderWidth: 0,
      borderBottomWidth: 0,
      borderColor: '#ccc',
      color: '#3a96ff',
      fontSize: 14,
      backgroundColor: '#fff',
      marginBottom: 0,
      borderRadius: 14,
    },
  });
