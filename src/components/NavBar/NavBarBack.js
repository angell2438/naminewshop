import React, {Component} from 'react';
import PropTypes from 'prop-types';

import NavBar from 'app/components/NavBar';
import ButtonIcon from 'app/components/ButtonIcon';

import {Actions} from 'react-native-router-flux';

export default class NavBarBack extends Component {

  static propTypes = {
    title: PropTypes.string
  }

  leftContent() {
    return (
      <ButtonIcon
        name='arrow_left'
        onPress={Actions.pop}
      />
    )
  }

  render() {
    const {props} = this;
    return (
      <NavBar
        title={props.title}
        leftContent={this.leftContent()}
        rightContentShow={false}
      />
    )
  }
}