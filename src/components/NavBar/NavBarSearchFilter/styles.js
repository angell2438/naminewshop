import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({

    wrap: StyleSheet.flatten([
      {
        backgroundColor: styles.COLOR_GREY_LIGHT,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
      },

    ]),

    navbar: StyleSheet.flatten([
      {
        shadowOpacity: 0,
        position: 'relative'
      },

    ]),

    wrapper: StyleSheet.flatten([
        {
            marginHorizontal: 10,
            flexDirection: 'row',
            alignItems: 'center',
            overflow: 'hidden'

        },

    ]),
  });

