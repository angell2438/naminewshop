import React, {Component} from 'react';
import PropTypes from 'prop-types';

import NavBar from 'app/components/NavBar'

export default class NavBarWithoutAction extends Component {

  static propTypes = {
    title: PropTypes.string
  }

  render() {
    const {props} = this;
    return (
      <NavBar
        title={props.title}
        leftContentShow={false}
        rightContentShow={false}
      />
    )
  }
}