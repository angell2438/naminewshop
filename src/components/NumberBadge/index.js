import React, { Component } from 'react';
import {
  View,
  Text
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

class NumberBadge extends Component {

  static propTypes = {
    number: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    style: PropTypes.object
  }

  static defaultProps = {
    style: {}
  }

  render() {
    const { props } = this;
    const style = styles(props);

    return (

      <View style={style.container}>
        <Text style={style.text}>
          {props.number}
        </Text>
      </View>

    )
  }
}


export default NumberBadge;