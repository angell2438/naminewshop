import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        width: styles.GUTTER,
        height: styles.GUTTER,
        backgroundColor: styles.COLOR_RED,
        justifyContent: 'center',
        borderRadius: 50
      },
      props.style && {
        ...props.style
      }
    ]),

    text: StyleSheet.flatten([
      {
        fontSize: 12,
        color: "#fff",
        fontWeight: '700',
        textAlign: 'center',
      }
    ]),
  });

