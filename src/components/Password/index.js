import React from 'react';
import {
    View
} from 'react-native';
import {
    Actions
} from 'react-native-router-flux';

import PropTypes from 'prop-types';


import Field from 'app/components/Field'
import Checkbox from 'app/components/Checkbox';
import Link from 'app/components/Link';

import * as routes from "app/config/sceneKeys";




import styles from './styles';



export default class Password extends React.Component {


    static propTypes = {
        placeholder: PropTypes.string,
        nameField: PropTypes.string,
        value: PropTypes.string,
        icon: PropTypes.string,
        textShow: PropTypes.string,
        textLink: PropTypes.string,
        color: PropTypes.string,

        checked: PropTypes.bool,
        onChangeInput: PropTypes.func,
        changePassword: PropTypes.func,
        visiblePassword: PropTypes.func,
    }

    static defaultProps = {
        placeholder: 'Пароль',
        value: '',
        nameField: 'password',
        icon: 'checked',
        textShow: 'Показать пароль',
        textLink: 'Изменить',
        checked: false,

    }


    changePassword = () => {
        Actions[routes.PASSWORD_RECOVERY.key].call();
    }

    render() {
        const { props } = this;

        const style = styles();


        return (
            <View>
                <Field
                    placeholder={props.placeholder}
                    value={props.value}
                    name={props.nameField}
                    icon={props.icon}
                    callback={props.onChangeInput}
                />

                <View style={style.bottomLine}>
                    <Checkbox
                        checked={props.checked}
                        text={props.textShow}
                        onPress={props.visiblePassword}
                    />
                    <Link
                        text={props.textLink}
                        onPress={props.changePassword || this.changePassword}
                    />
                </View>
            </View>
        )
    }


}