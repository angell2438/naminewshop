import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';
import PropTypes from 'prop-types';
import moment from 'moment';

import RatingCustom from 'app/components/RatingCustom'
import ButtonIcon from 'app/components/ButtonIcon'
import ProductStatus from 'app/components/ProductStatus'
import Counter from 'app/components/Counter'

import * as stylesConfig from 'app/config/style';

import styles from './styles';

class ProductCard extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    card: PropTypes.object.isRequired,
    viewStyle: PropTypes.string,
    index: PropTypes.number,
    type: PropTypes.string,
    showClose: PropTypes.bool,
    showQuantity: PropTypes.bool,
    onRemove: PropTypes.func,
    onAdd: PropTypes.func,
    onPress: PropTypes.func,
    // styling
    isWhite: PropTypes.bool,
    isGreen: PropTypes.bool,


  }

  static defaultProps = {
    showClose: false,
    showQuantity: false,
  }

  state = {
    imageWidth: 92
  }

  componentWillMount() {
    this.setImageSize();
  }


  render() {
    const { props, state } = this;
    const { card } = props;
    const style = styles({
      ...props,
      imageWidth: state.imageWidth,
      card
    });

    let deliveryStatus = '';


    switch (card.deliveryStatus) {
      case 'self-delivery':
        deliveryStatus = 'Ожидает в пункте самовывоза';
        break;
      case 'received':
        deliveryStatus = 'Получен';
    }

    return (
      <TouchableOpacity
        onPress={this.onPress}
        style={style.container}
        activeOpacity={.8}
      >
        {props.type == 'shortInfo' &&
          <View style={style.headInfo}>
            <Text style={style.smallText}>
              Код {card.code}
            </Text>
            <Text style={style.smallText}>
              {moment.unix(card.date).format("MM/DD/YYYY")}
            </Text>
          </View>
        }
        <View style={style.leftContent}>
          {card.status &&
            <ProductStatus
              type={card.status}
              style={style.status}
            />
          }
          <View style={style.imageContainer}>
            <AutoHeightImage
              width={state.imageWidth}
              source={{ uri: card.image }}
            />
          </View>

        </View>

        <View style={style.detail}>

          <View style={style.detailTop}>

            <View style={style.detailTopContainer}>

              <Text
                style={style.title}
                numberOfLines={2}
              >
                {card.name}
              </Text>

            </View>
            {props.showClose &&
              <ButtonIcon
                name='close'
                onPress={props.onRemove}

              />
            }
          </View>

          {props.type == 'shortInfo' &&
            <Text style={style.deliveryStatus}>
              {deliveryStatus}
            </Text>
          }

          {props.type !== 'shortInfo' &&
            <View style={style.detailBottom}>
              {!props.showQuantity && card.special &&
                <Text
                  style={style.oldPrice}
                >
                  {card.price} {card.currency}
                </Text>
              }
              {!props.showQuantity &&
                <View style={style.detailBottomContainer}>
                  <Text style={style.price}>
                    {card.special ? card.special : card.price} {card.currency}
                  </Text>
                  <ButtonIcon
                    name='more'
                    onPress={props.onAdd}
                  />
                </View>
              }
              {props.showQuantity ?
                <View style={style.counterInfo}>
                  <Counter
                    quantity={card.quantity}
                    currentId={card.product_id}
                    card={card}
                  />
                  <Text style={style.counterPrice}
                  >
                    {card.special ? card.special : card.price} {card.currency}
                  </Text>
                </View>
                :
                <View style={style.socialInfo}>
                  <RatingCustom
                    active={card.rating}
                    readMode={true}
                    style={style.rating}
                  />
                  <Text style={style.reviewText}>
                    {card.reviews ? card.reviews.length : 0} отзывов
                  </Text>
                </View>
              }
            </View>
          }

        </View>

        {props.type == 'shortInfo' &&
          <View style={style.totalContainer}>
            <Text style={style.bigText}>Итого к оплате:</Text>
            <Text style={style.bigText}>{card.price} {card.currency}</Text>
          </View>
        }

      </TouchableOpacity>
    )
  }


  setImageSize = () => {
    const { props, state } = this;
    const screenWidth = Dimensions.get('window').width;
    let imageWidth = state.imageWidth;

    switch (props.viewStyle) {
      case 'small':
        imageWidth = (screenWidth / 2) - (stylesConfig.GUTTER * 4);
        break;
      case 'big':
        imageWidth = screenWidth - (stylesConfig.GUTTER * 10);
        break;
      default:
        imageWidth = 92
    }

    this.setState({
      imageWidth
    })
  }


  onPress=()=>{
    const {props} = this;

    if(typeof props.onPress == 'function'){
      props.onPress(props.card.product_id);
    }
  }
}


export default ProductCard;