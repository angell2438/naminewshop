import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
    StyleSheet.create({
        container: StyleSheet.flatten([
            {
                flexBasis: '100%',
                flexDirection: 'row',
                alignItems: 'stretch',
                borderBottomWidth: 1,
                borderColor: styles.COLOR_GREY_MIDDLE,
                height: 122,
                backgroundColor: '#fff',
                // backgroundColor: 'yellow',
                padding: styles.GUTTER,
            },

            props.viewStyle == 'small' && {
                flexBasis: '50%',
                flexWrap: 'wrap',
                alignItems: 'flex-start',
                alignContent: 'space-between',
                height: 230,
            },
            props.viewStyle == 'small' &&
            props.index % 2 > 0 &&
            {
                borderRightWidth: 1
            },

            props.viewStyle == 'big' && {
                flexWrap: 'wrap',
                alignItems: 'flex-start',
                alignContent: 'space-between',
                height: 290,
            },

            props.type == 'shortInfo' && {
                flexWrap: 'wrap',
                height: 'auto'
            }
        ]),

        leftContent: StyleSheet.flatten([
            {
                flexBasis: props.imageWidth + styles.GUTTER,
                justifyContent: 'center',
                flexShrink: 0,
                position: 'relative',
                overflow: 'hidden',
                // backgroundColor: 'blue',
            },

            props.viewStyle == 'small' && {
                flexBasis: '100%',
                justifyContent: 'flex-end',
                alignItems: 'center',
                height: 95,
                marginBottom: styles.GUTTER
            },

            props.viewStyle == 'big' && {
                flexBasis: '100%',
                justifyContent: 'flex-end',
                alignItems: 'center',
                height: 150,
                marginBottom: styles.MARGIN
            },
        ]),

        status: StyleSheet.flatten([
            {
                position: 'absolute',
                left: 0,
                top: 0,
                zIndex: 2
            },

        ]),

        detail: StyleSheet.flatten([
            {
                justifyContent: 'space-between',
                flex: 1,
                // backgroundColor: 'orange'
            },

            props.viewStyle == 'small' && {
                flexBasis: '100%',
            },

            props.viewStyle == 'big' && {
                flexBasis: '100%',
            },

            props.type == 'shortInfo' && {
                justifyContent: 'center'
            }
        ]),

        detailTop: StyleSheet.flatten([
            {
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                paddingBottom: 5,
            }
        ]),

        detailTopContainer: StyleSheet.flatten([
            {
                justifyContent: 'space-between',
                flexShrink: 1,
            },

        ]),

        detailBottom: StyleSheet.flatten([
            {
                // flexDirection:'row',
                // justifyContent: 'space-between',
                // alignItems: 'space-between'
            },

        ]),

        detailBottomContainer: StyleSheet.flatten([
            {
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
            },

        ]),

        socialInfo: StyleSheet.flatten([
            {
                flexDirection: 'row',
                justifyContent: 'flex-start'
            },

        ]),

        counterInfo: StyleSheet.flatten([
            {
                flexDirection: 'row',
                justifyContent: 'space-between'
            },

        ]),

        reviewText: StyleSheet.flatten([
            {
                ...stylesGlobal.text_small,
                fontSize: 10,
            },

        ]),

        oldPrice: StyleSheet.flatten([
            {

                ...stylesGlobal.text_small,
                fontSize: 10,
                textDecorationLine: 'line-through',
                paddingBottom: 5
            },

        ]),

        price: StyleSheet.flatten([
            {
                color: styles.COLOR_BLACK,
                fontSize: styles.FONT_SIZE + 2,
                fontWeight: '700'
            },

        ]),
        counterPrice: StyleSheet.flatten([
            {
                color: styles.COLOR_BLACK,
                fontSize: styles.FONT_SIZE + 2,
                fontWeight: '700',
                alignSelf: 'center'
            },

        ]),

        title: StyleSheet.flatten([
            {
                color: styles.COLOR_BLACK,
                fontSize: 14,
                fontWeight: '400',
            },

        ]),


        rating: StyleSheet.flatten([
            {
                marginRight: 10
            },

        ]),

        headInfo: StyleSheet.flatten([
            {
                justifyContent: 'space-between',
                flexBasis: '100%',
                flexDirection: 'row',
                paddingBottom: styles.GUTTER
            },

        ]),

        totalContainer: StyleSheet.flatten([
            {
                justifyContent: 'space-between',
                flexBasis: '100%',
                flexDirection: 'row',
                paddingTop: styles.GUTTER
            },

        ]),

        smallText: StyleSheet.flatten([
            {
                fontSize: styles.FONT_SIZE - 4,
                fontWeight: '400',
                color: styles.COLOR_GREY
            },

        ]),

        bigText: StyleSheet.flatten([
            {
                fontSize: styles.FONT_SIZE + 2,
                fontWeight: '700',
                color: styles.COLOR_BLACK
            },

        ]),

        deliveryStatus: StyleSheet.flatten([
            {
                fontSize: styles.FONT_SIZE - 4,
                fontWeight: '500',
                color: styles.COLOR_GREEN,
                paddingTop: styles.GUTTER - 10
            },

            props.card.deliveryStatus == 'self-delivery' && {
                color: styles.COLOR_RED
            }
        ]),


    });

