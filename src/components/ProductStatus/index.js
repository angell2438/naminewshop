import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

export default class ProductStatus extends Component {

  static propTypes = {
    type: PropTypes.string.isRequired
  }




  render() {
    const { props, state } = this;
    const style = styles(props);
    let text = '';

    switch (props.type) {
      case "stock":
        text = 'Акция'
        break;
      case "top":
        text = 'Топ продаж'
        break;
      case "new":
        text = 'Новинка'
        break;
    }

    return (
      <View style={style.container}>
        <Text style={style.text}>
          {text}
        </Text>
      </View>
    )
  }
}