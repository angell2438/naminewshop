import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        paddingVertical: styles.GUTTER - 10,
        paddingHorizontal: styles.GUTTER - 5,
        borderRadius: 15
      },
      props.style && {
        ...props.style
      },
      props.type == 'stock' && {
        backgroundColor: styles.COLOR_RED
      },
      props.type == 'top' && {
        backgroundColor: styles.COLOR_ORANGE
      },
      props.type == 'new' && {
        backgroundColor: styles.COLOR_GREEN
      }
    ]),
    text: StyleSheet.flatten([
      {
        color: '#fff',
        fontSize: styles.FONT_SIZE - 4,
        fontWeight: '700',
      },
    ])
  });

