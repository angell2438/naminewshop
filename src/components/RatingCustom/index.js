import React, { Component } from 'react';
import {
  View,
  Text
} from 'react-native';
import PropTypes from 'prop-types';

import Rating from 'react-native-rating-simple';

import * as stylesConfig from 'app/config/style';

import styles from './styles';

export default class RatingCustom extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: props.active,
      text: ''
    }
  }

  static propTypes = {
    active: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    maxRating: PropTypes.number,
    readMode: PropTypes.bool,
    fullView: PropTypes.bool
  }

  static defaultProps = {
    active: 0,
    maxRating: 5,
    readMode: false,
    fullView: false
  }

  componentWillMount() {
    this.setDefaultRating(this.props);
  }

  componentWillReceiveProps(newtProps) {
    this.setDefaultRating(newtProps);
  }

  render() {
    const { props, state } = this;
    const styleProp = {
      height: props.fullView ? 30 : 10,
      margin: props.fullView ? 10 : 5,
    };
    const style = styles({
      ...props,
      ...styleProp
    });

    // todo: Change icon
    const propRating = {
      // Styling
      emptyStar:
        <View style={style.icon} />,
      halfStar:
        <View style={style.icon}>
          <View
            style={{
              width: '50%',
              height: styleProp.height - 2,
              backgroundColor: stylesConfig.COLOR_RED,
            }}
          />
        </View>,
      fullStar:
        <View style={{
          ...style.icon,
          backgroundColor: stylesConfig.COLOR_RED,
        }}
        />,
      starSize: styleProp.height,
      // END Styling

      maxRating: props.maxRating,
      rating: state.active,
      viewOnly: props.readMode,
      onChange: this.onChange,
      onChangeMove: this.onChangeMove,
    }

    return (
      <View style={style.container}>

        <Rating
          {...propRating}
        />

        {props.fullView &&
          <Text style={style.text}>
            {state.text}
          </Text>
        }

      </View>
    )
  }


  fetchText = (rating) => {
    const dataRating = [
      // {
      //   number: 0,
      //   text: 'Caмый ужасный'
      // },
      {
        number: .5,
        text: 'Ужасный'
      },
      {
        number: 1,
        text: 'Плохой'
      },
      {
        number: 1.5,
        text: 'Ну такое...'
      },
      {
        number: 2,
        text: 'Терпимый'
      },
      {
        number: 2.5,
        text: 'Средний'
      },
      {
        number: 3,
        text: 'Годный'
      },
      {
        number: 3.5,
        text: 'Хороший'
      },
      {
        number: 4,
        text: 'Крутой'
      },
      {
        number: 4.5,
        text: 'Лучший'
      },
      {
        number: 5,
        text: 'Самый лучший'
      },

    ];

    const ratingObject = dataRating.find(data => data.number == rating);


    this.setState({
      text: ratingObject ? ratingObject.text : ''
    })


  }

  setDefaultRating = (props) => {
    const rating = Number(props.active);
    let newRating = 0;

    for (let i = 0; i <= props.maxRating; i++) {

      if (i == rating) {
        newRating = rating;
        return
      }

      if (rating > i && rating < i + 1) {
        newRating = i + '.5'
      }
    }

    this.setState({
      active: Number(newRating)
    });

    this.fetchText(Number(newRating))
  }

  onChange = (rating) => {
    this.setState({ active: rating });

    this.fetchText(rating)
  }

  onChangeMove = (rating) => {
    this.fetchText(rating)
  }
}