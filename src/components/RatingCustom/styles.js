import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        marginHorizontal: -props.margin/2,
      },
      props.style && {
        ...props.style
      }
    ]),


    icon: StyleSheet.flatten([
      {
        borderWidth: 1,
        width: props.height,
        height: props.height,
        marginHorizontal: props.margin/2,
        borderColor: styles.COLOR_RED
      },

    ]),

    text: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE - 2,
        textAlign: 'center',
        flexBasis: '100%',
        height: 20,
        marginHorizontal: props.margin/2,
        marginTop: 10,
        color: styles.COLOR_RED
      },

    ]),



  });

