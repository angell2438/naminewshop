import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

export default class Review extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  static defaultProps = {
    showButton: true
  }

  state = {
    showAll: false,
    showButton: false,
    showButtonNumber: 50,
    lineCount: 4,
  }

  render() {
    const { props, state } = this;
    const style = styles(props);

    const textProps = {
      style: style.text
    }

    if (!state.showAll) {
      textProps.numberOfLines = state.lineCount
    }

    return (

      <View style={style.container}>
        <View style={style.header} >
          <Text style={style.name}>
            {props.data.name}
          </Text>
          <Text style={style.date}>
            {props.data.date}
          </Text>
        </View>
        <View
          style={style.content}
          onLayout={(event) => this.getSizes(event)}
        >
          <Text {...textProps}>
            {props.data.text}
          </Text>
        </View>

        {state.showButton
          && props.showButton &&
          <View style={style.actions}>
            <TouchableOpacity
              onPress={this.onPress}
              style={style.btn}
              activeOpacity={.8}
            >

              <Text style={style.btnText}>
                {state.showAll ? 'Скрыть' : 'Читать далее'}
              </Text>

            </TouchableOpacity>
          </View>
        }

      </View>

    )
  }

  getSizes(event) {
    const { state } = this;

    if (event.nativeEvent.layout.height > state.showButtonNumber) {
      this.setState({
        showButton: true
      })
    }
  }



  onPress = () => {
    this.setState({
      showAll: !this.state.showAll
    })
  }

}