import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        paddingHorizontal: styles.GUTTER,
        paddingVertical: styles.GUTTER - 5,
        ...stylesGlobal.borderBottom
      },

    ]),

    header: StyleSheet.flatten([
      {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 6
      }
    ]),

    name: StyleSheet.flatten([
      {
        fontWeight: '500',
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE,
      }
    ]),

    date: StyleSheet.flatten([
      {
        color: styles.COLOR_GREY,
        fontSize: styles.FONT_SIZE - 4,
        fontWeight: "400",
      },

    ]),

    text: StyleSheet.flatten([
      {
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE - 2,
        fontWeight: "400"
      },

    ]),

    actions: StyleSheet.flatten([
      {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingTop: styles.GUTTER - 5
      },

    ]),

    btnText: StyleSheet.flatten([
      {
        color: styles.COLOR_BLUE_LIGHT,
        fontSize: styles.FONT_SIZE,
        fontWeight: "400"
      },

    ]),

  });

