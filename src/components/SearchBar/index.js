import React, {Component} from 'react';

import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  Text,
  Keyboard, StyleSheet
} from 'react-native';

import styles from './styles';

import Field from 'app/components/Field';
import * as routes from "app/config/sceneKeys";
import * as Animatable from 'react-native-animatable';
import Icon from 'app/components/Icon';
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {getSearchList} from "app/redux/actions/search";

class SearchBar extends Component {

  static propTypes = {
    title: PropTypes.string,
    viewStyle: PropTypes.string,
    autoFocus: PropTypes.bool
  }


  constructor() {
    super();

    this.state = {
      fields: {
        searchString: '',
        isLoading: false,
        message: '',
        startArray: [],
        searchFields: ['name'],
      },
      unfocus: false,
      inFocus: false,
      phMove: false,
      placeholder: 'Поиск'
    }
  }

  componentDidMount() {

    // this.props.getSearchList(this.state.results);
  }

  componentWillReceiveProps(nextProps) {
    const {props, state} = this;
    if (nextProps.unfocus !== props.unfocus) {
      this.setState({
        unfocus: nextProps.unfocus
      })
    }

    props.getSearchList(state.results);
  }

  render() {
    const {props, state} = this;
    const style = styles(props);
    return (
      <View style={[style.container, this.props.style, this.props.inverted && {backgroundColor: '#ccc'}]}>
        <View style={style.wrapper}>
          <View style={[style.icon, {transform: [{translateX: this.state.hasFocus ? 70 : 0}]}]}>
            <Icon
              name='search'
              color='#e83c4d'
            />
          </View>
          <Animatable.View
            useNativeDriver
            transition={'translateX'}
            style={{
              alignSelf: 'center',
              flex: 1,
              transform: [{translateX: this.state.inFocus ? -70 : 0}]
            }}
          >
            <Field
              placeholder={state.placeholder}
              value={state.fields.searchString}
              name="search"
              callback={this.onSearchedList}
              animatedFocus={true}
              onFocus={() => {
                this.openSearchLightbox();
                this.setState({inFocus: true, placeholder: ''});
              }}
              styleType='search'
              customStyle={StyleSheet.flatten([{}, {paddingHorizontal: 30}])}
              unfocus={state.unfocus}
            />
          </Animatable.View>
        </View>
        <Animatable.View
          style={{marginRight: -70, width: 70, transform: [{translateX: this.state.inFocus ? -70 : 0}]}}
          useNativeDriver
          transition={'translateX'}>
          <TouchableOpacity
            activeOpacity={.8}
            onPress={() => {
              this.setState({inFocus: false, phMove: false});
              this.closeSearchBar();
            }}
            style={style.btnSearch}
          >
            <Text style={{color: 'red', fontSize: 16}}>Отмена</Text>
          </TouchableOpacity>
        </Animatable.View>
      </View>
    );
  }

  onSearchedList = ({value = ''}) => {
    const {props, state} = this;
    let results = [];

    if (value.length == 0) {
      return props.getSearchList(results)
    } else if (value.length >= 2) {
      props.searchArray.filter(coll => {
        if (coll.name.toLowerCase()
          .indexOf(value.toLowerCase()) !== -1) {

          let parts = coll.name.split(new RegExp(`(${value.trim()})`, 'gi'));

          let partColl = parts
            .filter((part) => part !== '')
            .map((part, i) =>
              <Text key={i} style={part.toLowerCase() === value.toLowerCase() ? {
                fontWeight: 'bold',
                color: '#000'
              } : {}}>
                {part}
              </Text>
            );
          results.push(partColl)
        }
      });

    }
    props.getSearchList(results)

  };


  openSearchLightbox = () => {
    Actions[routes.SHEARCH_LIGHTBOX.key]()
  }

  closeSearchBar = () => {
    // this.setState({unfocus: true})
    Keyboard.dismiss();
    Actions.pop()
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = {
  getSearchList
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar)
