import {StyleSheet} from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    MainContainer: {
      justifyContent: 'center',
      flex: 1,
      margin: 7,

    },
    description: {
      marginBottom: 20,
      fontSize: 18,
      textAlign: 'center',
      color: '#656565'
    },
    searchInput: {
      height: 36,
      padding: 4,
      marginRight: 5,
      flex: 4,
      fontSize: 18,
      borderWidth: 1,
      borderColor: '#48BBEC',
      borderRadius: 8,
      color: '#48BBEC'
    },
    image: {
      width: 217,
      height: 138
    },
    container: StyleSheet.flatten([
      {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'transparent',
      },

    ]),
    wrapper: StyleSheet.flatten([
      {
        marginHorizontal: 0,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 15,
        overflow: 'hidden'
      },

    ]),
    btnSearch: StyleSheet.flatten([
      {
        marginLeft: 10
      },

    ]),
    field: {
      paddingHorizontal: 15
    },

    inputSearch: {
      borderColor: '#ccc',
      borderWidth: 1,
      borderRadius: 0,
      color: '#000'
    },
    icon: StyleSheet.flatten([
      {
        fontSize: 21,
        flexShrink: 0,
        marginRight: styles.MARGIN / 2,
        borderRadius: 5,
        position: 'absolute',
        left: 0,
        zIndex: 10,
        top: 18
      },
    ]),
    ContainerSortList: {
      flex: 1,
      // justifyContent: 'center',
      // alignItems: 'center',
      width: '100%',
      height: 250,
      backgroundColor: '#fff',
      marginTop: 100
    },
    SortListWrapper: {
      // justifyContent: 'center',
      // alignItems: 'center',
      height: 250,
      width: '100%',
      borderWidth: 0,
      position: 'absolute',
      top: 0,
      overflow: 'hidden',
      backgroundColor: 'red'
    },

  });

