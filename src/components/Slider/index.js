import React, { Component, Fragment } from 'react';
import {
  View,
  Dimensions
} from 'react-native';

import PropTypes from 'prop-types';

import Swiper from 'react-native-swiper';
import AutoHeightImage from 'react-native-auto-height-image';

import * as stylesConfig from 'app/config/style';
import styles from './styles';


export default class Slider extends Component {

  static propTypes = {
    tabs: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      component: PropTypes.element.isRequired,
    }))
  }

  constructor() {
    super();

    this.state = {
      imageWidth: 0
      // imagesSizes: []
    }
  }

  componentWillMount() {
    const imageWidth = Dimensions.get('window').width - (stylesConfig.GUTTER * 10);
    this.setState({
      imageWidth
    });
    // this.setImageParams()
  }

  // setImageParams = () => {
  //   const { props } = this;
  //   const imageWidth = Dimensions.get('window').width - (stylesConfig.GUTTER * 10);
  //   const images = [...props.images];
  //   const imagesParams = [];

  //   images.map(image => {
  //     imagesParams.push({
  //       url: image,
  //       width: imageWidth
  //     })
  //   })

  //   this.setState({
  //     imagesParams
  //   })
  // }

  // setImageSizes = (item, height) => {
  //   const { state } = this;
  //   const num = height - 150;
  //   const imagesParams = [...state.imagesParams];


  //   console.log('height ', item)


  //   if (height && num > 0) {
  //     //   console.log('height - 150 ', height - 150);

  //     imagesParams.find(image => {
  //       if (image == item) {

  //         if(!image.height){
  //           console.log('item ', item)
  //           console.log('image ', image)
  //           console.log('height ', height)
  //           image.width = image.width - num
  //           image.height = height - num
  //         }
  //       }
  //     })

  //     //   this.setState({
  //     //     imagesParams
  //     //   })
  //   }


  // }

  render() {
    const { props, state } = this;
    const style = styles(props);


    return (
      <View style={props.style}>
        <Swiper
          style={style.wrapper}
          paginationStyle={style.pagination}
          dot={<View style={style.dot} />}
          activeDot={<View style={[style.dot, style.dotActive]} />}
        >
          {props.images.map((item, index) => <View style={style.slide} key={'slide' + index}>
            <AutoHeightImage
              width={state.imageWidth}
              source={{ uri: item }}

            />
          </View>
          )}
        </Swiper>
      </View>


    );
  }
}

