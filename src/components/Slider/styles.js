import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({

    wrapper: StyleSheet.flatten([
      {
        height:178,
        overflow: 'hidden'
        // flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor: '#92BBD9',
      }

    ]),

    slide: StyleSheet.flatten([
      {
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        paddingBottom: 8,
        // paddingBottom: styles.MARGIN+8,
      }

    ]),

    dot: StyleSheet.flatten([
      {
      width:8,
      height:8,
      borderWidth: 1,
      marginLeft: 5,
      borderRadius: 20,
      borderColor: styles.COLOR_BLACK
      }

    ]),

    dotActive: StyleSheet.flatten([
      {
      backgroundColor: styles.COLOR_RED,
      borderColor: styles.COLOR_RED
      }

    ]),

    pagination: StyleSheet.flatten([
      {
      bottom: 0,
      justifyContent: 'flex-end',
      paddingHorizontal: styles.GUTTER
      }

    ]),

  });

