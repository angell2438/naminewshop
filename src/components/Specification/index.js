import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

export default class Specification extends Component {


  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  render() {
    const { props, state } = this;
    const style = styles(props);

    return (

      <View style={style.container}>
        <Text style={style.title}>
          {props.data.title}
        </Text>
        <Text style={style.detail}>
          {props.data.detail}
        </Text>
      </View>

    )
  }

}