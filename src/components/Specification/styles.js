import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        flexDirection: 'row',
        padding: styles.GUTTER,
        ...stylesGlobal.borderBottom
      },

    ]),

    title: StyleSheet.flatten([
      {
        fontWeight: '500',
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE - 2,
        flexBasis: '36.2%'
      }
    ]),

    detail: StyleSheet.flatten([
      {
        fontWeight: '400',
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE - 2,
        paddingLeft: 25,
        flexBasis: '63.8%'
      }
    ])
  });

