import React, { Component, Fragment } from 'react';
import {
  Tabs,
  Tab,
  Container
} from 'native-base';
import {
  StatusBar,
  View,
  Text
} from 'react-native';

import PropTypes from 'prop-types';


import styles from './styles';


export default class TabBar extends Component {

  static propTypes = {
    tabs: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      component: PropTypes.element.isRequired,
    })),
    cb: PropTypes.func
  }

  render() {
    const { props } = this;
    const style = styles(props);

    return (
      <Tabs initialPage={0}
        tabBarUnderlineStyle={style.line}
        onChangeTab={(i) => this.cb(i)}
      >
        {props.tabs.map((tab, index) => (
          <Tab
            key={`tab-${index}`}
            heading={tab.title}
            textStyle={style.text}
            activeTextStyle={style.text__active}
          >
            {tab.component}
          </Tab>
        ))}

      </Tabs>


    );
  }

  cb = (data) => {
    const { props } = this;

    if (typeof props.cb == 'function') {
      props.cb(data)
    }
  }
}