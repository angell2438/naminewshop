import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';
import { COLOR_GREY } from '../../config/style';

export default props =>
  StyleSheet.create({

    line: StyleSheet.flatten([
      {
        backgroundColor: styles.COLOR_RED
      }

    ]),

    text: StyleSheet.flatten([
      {
        color: styles.COLOR_GREY,
        marginLeft: 0,
        marginRight: 0,
        fontWeight: '400'
      }

    ]),

    text__active: StyleSheet.flatten([
      {
        color: styles.COLOR_RED,
        marginLeft: 0,
        marginRight: 0,
        fontWeight: '400'
      }

    ]),

  });

