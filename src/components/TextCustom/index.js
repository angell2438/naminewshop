import React, { Component } from 'react';
import {
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

export default class TextCustom extends Component {


  static propTypes = {
    text: PropTypes.string.isRequired,
    type: PropTypes.string
  }


  render() {
    const { props } = this;
    const style = styles(props);

    return (
      <Text style={style.text}>
        {props.text}
      </Text>
    )
  }
}