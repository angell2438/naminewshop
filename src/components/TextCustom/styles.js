import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    text: StyleSheet.flatten([
      {
        // paddingHorizontal: styles.GUTTER,
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE - 2,
        textAlign: 'left',
        paddingBottom: styles.GUTTER + 5
      },
      props.type == 'title' && {
        fontSize: styles.FONT_SIZE,
        paddingBottom: styles.GUTTER
      },
      props.type == 'title_small' && {
        color: styles.COLOR_RED,
        paddingBottom: styles.GUTTER - 5
      },
    ])
  });

