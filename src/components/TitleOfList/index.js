import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

import Icon from 'app/components/Icon';

import * as stylesConfig from 'app/config/style';
import styles from './styles';

export default class TitleOfList extends Component {


  static propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    showRightIcon: PropTypes.bool,
    marginTop: PropTypes.bool
  }

  static defaultProps = {
    showRightIcon: false,
    marginTop: false,
  }



  render() {
    const { props } = this;
    const style = styles({ ...props });

    return (
      <TouchableOpacity
        onPress={this.onPress}
        style={style.container}
        activeOpacity={1}
      >
        <Text style={style.title}>
          {props.title}
        </Text>
        <Icon
          name='arrow_left'
          color={stylesConfig.COLOR_GREY}
        />

      </TouchableOpacity>
    )
  }

  onPress = () => {
    const { props } = this;

    if (typeof props.onPress == 'function') {
      props.onPress()
    }
  }
}