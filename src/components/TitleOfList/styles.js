import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        paddingHorizontal: styles.GUTTER,
        backgroundColor: styles.COLOR_GREY_LIGHT,
        borderBottomWidth: 1,
        borderColor: styles.COLOR_GREY_MIDDLE,
        height: 35,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
      props.style && {
        ...props.style
      },

      props.marginTop && {
        marginTop: 20
      },
    ]),


    title: StyleSheet.flatten([
      {
        // paddingHorizontal: styles.GUTTER,
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE,
        textAlign: 'left',
      }
    ])
  });

