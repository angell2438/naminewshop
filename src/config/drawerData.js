export default [
  {
    id: 'menu1',
    data: [
      {
        id: 'item1',
        scene: 'AUTH',
        icon: 'profile',
        title: 'Вход в магазин'
      },
      {
        id: 'item2',
        scene: 'INDEX',
        icon: 'home',
        title: 'Главная',
        active: true
      },
      {
        id: 'item3',
        scene: 'CATEGORIES',
        icon: 'list',
        title: 'Каталог товаров'
      },
      {
        id: 'item4',
        icon: 'discount',
        title: 'Акции'
      },
      {
        id: 'item5',
        icon: 'discount',
        title: 'Скидки'
      },
    ]
  },
  {
    id: 'menu2',
    data: [
      {
        id: 'item6',
        icon: 'arrow_left',
        scene: 'BASKET',
        title: 'Корзина',
        newCount: '1'
      },
      {
        id: 'item7',
        scene: 'FAVORITES',
        icon: 'heart_outline',
        title: 'Избранные',
        newCount: '2'
      },
      {
        id: 'item8',
        scene: 'MY_ORDERS',
        icon: 'arrow_left',
        title: 'Мои заказы',
        newCount: '28'
      },
    ]
  },
  {
    id: 'menu3',
    data: [
      {
        id: 'item9',
        scene: 'INFORMATION',
        icon: 'information',
        title: 'Информация'
      },
      {
        id: 'item10',
        scene: 'SETTINGS',
        icon: 'settings',
        title: 'Настройки'
      },
    ]
  },
];