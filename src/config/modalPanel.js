import { PRODUCTS } from 'app/redux/types';
export default {
  modalSort: {
    data: [
      {
        id: 'button1',
        scene: 'PRODUCTS',
        type: PRODUCTS.sortedRaring,
        title: 'По рейтенгу'
      },
      {
        id: 'button2',
        scene: 'PRODUCTS',
        type: PRODUCTS.sortedPopular,
        title: 'По популярности'
      },
    ]
  },
  modalProduct:{
    data: [
      {
        id: 'item6',
        icon: 'arrow_left',
        title: 'Корзина',
        newCount: '1'
      },
      {
        id: 'item7',
        scene: 'FAVORITES',
        icon: 'arrow_left',
        title: 'Избранные',
        newCount: '2'
      },
      {
        id: 'item8',
        icon: 'arrow_left',
        title: 'Мои заказы',
        newCount: '28'

      },
    ]
    },
};
