export const INDEX = {
  key: "INDEX",
  title: "Naomi Shop"
};

export const AUTH = {
  key: "AUTH",
  title: "Вход в магазин"
};

export const PASSWORD_RECOVERY = {
  key: "PASSWORD_RECOVERY",
  title: "Восстановление пароля"
};

export const FAVORITES = {
  key: "FAVORITES",
  title: "Избранные"
};

export const REVIEW = {
  key: "REVIEW",
  title: "Ваш отзыв"
};

export const PRODUCTS = {
  key: "PRODUCTS",
  title: "Продукты"
};

export const PRODUCT = {
  key: "PRODUCT",
  title: "Продукт"
};

export const CATEGORIES = {
  key: "CATEGORIES",
  title: "Каталог"
};

export const SUB_CATEGORIES = {
  key: "SUB_CATEGORIES",
  title: "Подкатегория"
};

export const PROFILE = {
  key: "PROFILE",
  title: "Кабинет"
};

export const PROFILE_EDIT = {
  key: "PROFILE_EDIT",
  title: "Редактирование"
};

export const INFORMATION = {
  key: "INFORMATION",
  title: "Информация"
};

export const INFORMATION_DETAIL = {
  key: "INFORMATION_DETAIL",
  title: "Детальная информация"
};

export const SETTINGS = {
  key: "SETTINGS",
  title: "Настройки"
};

export const SHEARCH_LIGHTBOX = {
  key: "SHEARCH_LIGHTBOX",
  title: "Поиск"
};

export const BASKET = {
  key: "BASKET",
  title: "Корзина"
};

export const ORDER = {
  key: "ORDER",
  title: "Оформление заказа"
};

export const MY_ORDERS = {
  key: "MY_ORDERS",
  title: "Мои заказы"
};

export const FILTER = {
  key: "FILTER",
  title: "Фильтр"
};
