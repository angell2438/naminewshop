export const GUTTER = 15;
export const FONT_SIZE = 14;
export const RADIUS = 5;
export const INTENT = GUTTER*2;
export const MARGIN = 20;

export const FIELD_HEIGHT = 40;
export const ITEM_HEIGHT = 56;
export const NAVBAR_WITH_FILTER = 160;
export const NAVBAR_WITHOUT_FILTER = 120;


export const FONT_NAME = 'Geometria-Medium';

export const COLOR_BLACK = '#1a1a1a';
export const COLOR_GREY = '#808080';
export const COLOR_GREY_DARK = '#f0f0f0';
export const COLOR_GREY_MIDDLE = '#e6e6e6';
export const COLOR_GREY_LIGHT = '#f8f8f8';
export const COLOR_GREY_LIGHTER = '#cccccc';
export const COLOR_RED = '#e83c4d';
export const COLOR_BLUE = '#3854a1';
export const COLOR_BLUE_LIGHT = '#3a96ff';
export const COLOR_GREEN = '#4dc611';
export const COLOR_ORANGE = '#f58301';