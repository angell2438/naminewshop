import React, { Component } from 'react';
import {
    View,
    Text,
    Keyboard
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import Button from 'app/components/Button';
import Field from 'app/components/Field'
import Checkbox from 'app/components/Checkbox';

import * as routes from "app/config/sceneKeys";


import styles from './styles';


export default class Recovery extends Component {

    constructor() {
        super();

        this.state = {
            fields: {
                oldPassword: '',
                newPassword: '',
                repeatPassword: '',
            },
            checked: false,
            loading: false
        }
    }

    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <View style={style.container}>

                <Field
                    placeholder="Текущий пароль"
                    value={state.fields.oldPassword}
                    name="oldPassword"
                    icon="arrow_left"
                    callback={this.onChangeInput}
                />

                <Field
                    placeholder="Новый пароль"
                    value={state.fields.newPassword}
                    name="newPassword"
                    icon="arrow_left"
                    callback={this.onChangeInput}
                />

                <Field
                    placeholder="Повторить пароль"
                    value={state.fields.repeatPassword}
                    name="repeatPassword"
                    icon="arrow_left"
                    callback={this.onChangeInput}
                />

                <View style={style.additionalFunctional}>
                    <Checkbox
                        checked={state.checked}
                        text='Показать пароли'
                        onPress={this.check}
                    />
                </View>

                <Text style={style.text_middle}>
                    Пароль должен быть от 6 до 16 символов,
                    содержать цифры и заглавные буквы и не должен
                    совпадать с именем и email
                </Text>

                <Button
                    style={{ ...style.btn, ...style.btnBottomNone }}
                    onPress={this.onSubmit}
                    text='Сохранить'
                />

                <Button
                    style={{ ...style.btn, ...style.btnSecond }}
                    onPress={this.cancel}
                    text='Отмена'
                    isWhite={true}
                />


            </View>
        )
    }

    check = () => {
        this.setState({
            checked: !this.state.checked
        })
    }

    onSubmit = () => {
        Keyboard.dismiss();
        this.setState({ loading: true });

    }

    onChangeInput = (data) => {
        const { state } = this;
        this.setState({
            fields: {
                ...state.fields,
                [data.name]: data.value
            }
        });
    }

    cancel = () => {
        Actions[routes.AUTH.key].call();
    }
}