import React from 'react';
import {
    View,
    Text,
    Keyboard
} from 'react-native';

import {
    Actions
} from 'react-native-router-flux';

import * as routes from "app/config/sceneKeys";

import Password from 'app/components/Password';


import Button from 'app/components/Button';
import Field from 'app/components/Field'
import Checkbox from 'app/components/Checkbox';
import Link from 'app/components/Link';
import ButtonSocial from 'app/components/ButtonSocial';

import styles from './styles';



class SignIn extends React.Component {

    constructor() {
        super();

        this.state = {
            fields: {
                login: '',
                password: '',
            },
            checked: false,
            loading: false
        }
    }

    componentDidMount() {

    }


    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <View style={style.container}>

                <Text style={style.title}>Вход</Text>

                <Field
                    placeholder="Введите телефон или email"
                    value={state.fields.login}
                    name="login"
                    icon="profile"
                    callback={this.onChangeInput}
                />

                <Password
                    velue={state.fields.password}
                    checked={state.checked}
                    textLink='Забыли пароль'
                    visiblePassword={this.visiblePassword}
                    onChangeInput={this.onChangeInput}
                />

                <Button
                    style={style.btn}
                    onPress={this.onSubmit}
                    text='Войти'
                />

                <Text style={style.text}>Войти через соцсети</Text>

                <View style={style.social}>
                    <ButtonSocial
                        type='facebook'
                        text='Facebook'
                    />
                    <ButtonSocial
                        type='google'
                        text='Google+'
                    />
                </View>

            </View>
        )
    }

    visiblePassword = () => {
        this.setState({
            checked: !this.state.checked
        })
    }

    onSubmit = () => {
        Keyboard.dismiss();
        this.setState({ loading: true });


    }

    onChangeInput = (data) => {
        const { state } = this;
        this.setState({
            field: {
                ...state.field,
                [data.name]: data.value
            }
        });
    }

    forgotPassword = () => {
        Actions[routes.PASSWORD_RECOVERY.key].call();
    }
}

export default SignIn;