import React from 'react';
import {
    View,
    Text,
    Keyboard
} from 'react-native';

import Button from 'app/components/Button';
import Field from 'app/components/Field'
import Checkbox from 'app/components/Checkbox';
import Link from 'app/components/Link';


import styles from './styles';



class SignUp extends React.Component {

    constructor() {
        super();

        this.state = {
            fields: {
                fullname: '',
                email: '',
                password: '',
            },
            checked: false,
            loading: false
        }
    }

    componentDidMount() {

    }

    check = () => {
        this.setState({
            checked: !this.state.checked
        })
    }

    onSubmit = () => {
        Keyboard.dismiss();
        this.setState({ loading: true });

        this.props.signIn(this.props.auth)
            .then(Actions.main)
            .catch(error => alert(error))
            .finally(() => {
                this.setState({ loading: false });
            })
    }

    onChangeInput = (data) => {
        const { state } = this;
        this.setState({
            fields: {
                ...state.fields,
                [data.name]: data.value
            }
        });
    }

    agreeAction = () => {
        alert('agree click')
    }

    renderForm() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <View style={style.container}>

                <Text style={style.title}>Регистрация</Text>

                <Field
                    placeholder="Имя и фамилия"
                    value={state.fields.fullname}
                    name="fullname"
                    icon="profile"
                    callback={this.onChangeInput}
                />

                <Field
                    placeholder="Email"
                    value={state.fields.email}
                    name="email"
                    icon="profile"
                    callback={this.onChangeInput}
                />

                <Field
                    placeholder="Пароль"
                    value={state.fields.password}
                    name="password"
                    icon="checked"
                    callback={this.onChangeInput}
                />

                <View style={style.additionalFunctional}>
                    <Checkbox
                        checked={state.checked}
                        text='Показать пароль'
                        onPress={this.check}
                    />
                </View>

                <Text style={style.text_middle}>
                    Пароль должен быть от 6 до 16 символов,
                    содержать цифры и заглавные буквы и не должен
                    совпадать с именем и email
                </Text>

                <Button
                    style={style.btn}
                    onPress={this.onSubmit}
                    text='Зарегистрироваться'
                />

                <Text style={style.text}>Регистрируясь, вы соглашаетесь</Text>
                <Link
                    text='с пользовательским соглашением'
                    blue={true}
                    center={true}
                    onPress={this.agreeAction}
                />


            </View>
        )
    }

    render() {
        const { state } = this;
        return (
            <View>
                {state.loading &&
                    <Text>Loading...</Text>
                }
                {!state.loading &&
                    this.renderForm()
                }
            </View>
            // <Container style={{ justifyContent: 'center' }}>
            //     {
            //         this.state.loading ? <Spinner color='red' /> : this.renderForm()
            //     }
            // </Container>
        )
    }
}

export default SignUp;