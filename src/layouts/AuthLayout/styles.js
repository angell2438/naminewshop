import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        ...stylesGlobal.container,
        paddingTop: styles.GUTTER-5
      }

    ]),

    title: StyleSheet.flatten([
      {
        ...stylesGlobal.title,
        textAlign: 'center',
        paddingTop: styles.GUTTER+5,
        marginBottom: styles.GUTTER / 2
      },

    ]),

    additionalFunctional: StyleSheet.flatten([
      {
        flexDirection: 'row',
        justifyContent: 'space-between',
      }
    ]),

    social: StyleSheet.flatten([
      {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: styles.MARGIN / 2
      }
    ]),

    text: StyleSheet.flatten([
      {
        ...stylesGlobal.text,
        textAlign: 'center'
      },

    ]),

    text_middle: StyleSheet.flatten([
      {
        ...stylesGlobal.text_middle,
        textAlign: 'center',
        paddingHorizontal: styles.GUTTER*2,
        marginTop: styles.MARGIN,
        marginBottom: styles.MARGIN / 2
      },

    ]),

    btn: StyleSheet.flatten([
      {
        marginTop: styles.MARGIN,
        marginBottom: styles.GUTTER * 2
      },

    ]),

    btnSecond: StyleSheet.flatten([
      {
        marginTop: styles.GUTTER,
        marginBottom: 0
      },

    ]),

    btnBottomNone: StyleSheet.flatten([
      {
        marginBottom: 0
      },

    ]),


  });

