import React, { Component } from 'react';
import {
    View,
    Image,
    Text
} from 'react-native';

import PropTypes from 'prop-types';

import styles from './styles';


export default class Background extends Component {

    static propTypes = {
        viewStyle: PropTypes.string,
    };

    constructor() {
        super();

        this.state = {
            isLoading: false
        }

    }
    render() {
        const { props, state } = this;
        const style = styles(props);
        return (
            <View style={style.containerBackground}>
                <View style={style.logoContainer}>
                    <Image style={style.logo} source={require('app/img/Artboard.png')} />
                </View>
                <Text style={style.boldText}>В корзине</Text>
                <Text style={style.normalText}>нет товаров</Text>
            </View>

        )
    }
}

