import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
} from 'react-native';

import PropTypes from 'prop-types';
import ProductCard from 'app/components/ProductCard';

import styles from './styles';
import BasketPanel from "app/components/BasketPanel";

export default class BasketResult extends Component {

    static propTypes = {
        viewStyle: PropTypes.string,
    };

    constructor() {
        super();

        this.state = {
            isLoading: false
        }

    }




    render() {
        const { props, state } = this;
        const style = styles(props);
        return (
            <View style={style.wrapperContent}>
                <View style={style.container}>
                    {props.checkoutArray.map(card =>
                        <ProductCard
                            key={card.id}
                            card={card}
                            showClose={true}
                            showQuantity={true}
                            onRemove={()=> this.onRemove(card.id)}
                        />
                    )}

                </View>
                <BasketPanel totalPrice={this.calculation()}/>

            </View>
        )
    }
    calculation = () => {
        const { props } = this;
        const msgTotal = props.checkoutArray.reduce(function(prev, card) {
            return prev + parseInt(`${card.newPrice ? card.newPrice : card.price}`.replace(/ /g, ''));
        }, 0);

        return msgTotal
    };
    onRemove = (id) => {
        const { props } = this;

        // const index = props.checkoutArray.find((item, i) => item.id == id ? i:false)

        props.remove(id)

    }
}

