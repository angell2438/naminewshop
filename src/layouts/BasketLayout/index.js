import React, { Component } from 'react';
import {
    Keyboard,
    ScrollView,
    View,

} from 'react-native';

import PropTypes from 'prop-types';

import { Actions } from 'react-native-router-flux';

import styles from './styles';

import Background from "./Background";
import BasketResult from "./BasketResult";


export default class BasketLayout extends Component {

    static propTypes = {
        viewStyle: PropTypes.string,
        status: PropTypes.bool,

    };

    static defaultProps= {
        status: false
    };

    constructor() {
        super();

        this.state = {
            isLoading: false,
            value: '',
        }

    }

    componentDidMount() {
        // console.log(this.props);
        // this.fetchData();
    }

    render() {
        const { props, state } = this;
        const style = styles(props);
        return (
            <View style={style.containerBasket}>
                {props.checkoutArray.length ?  <BasketResult checkoutArray={props.checkoutArray} remove={props.remove}/> : <Background/>}
            </View>

        )
    }
    onSubmit = () => {
        Keyboard.dismiss();

    }

}

