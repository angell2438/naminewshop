import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
    StyleSheet.create({
        containerBackground: StyleSheet.flatten([
            {
                width: '100%',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
            }

        ]),
        containerBasket: StyleSheet.flatten([
            {
                width: '100%',
                flex: 1,
                marginTop: 44,
            }

        ]),
        wrapperContent: StyleSheet.flatten([
            {
                flexDirection: 'column',
                flex: 1,

            }

        ]),
        container: StyleSheet.flatten([
            {

                ...stylesGlobal.container,
                flex: 1,
                paddingHorizontal:0
            }

        ]),

        logoContainer: StyleSheet.flatten([
            {
                flexShrink: 0,
                paddingHorizontal: styles.GUTTER,
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
                // height: props.heightLogoContainer,
                marginBottom: styles.GUTTER / 2,
            }
        ]),

        logo: StyleSheet.flatten([
            {
                height: 20
            }
        ]),

        boldText: StyleSheet.flatten([
            {
                color: styles.COLOR_BLACK,
                fontSize: styles.FONT_SIZE * 2,
                fontWeight: '500',
                marginBottom: styles.GUTTER / 2,
                textAlign: 'center'
            }
        ]),

        normalText: StyleSheet.flatten([
            {
                color: styles.COLOR_GREY_LIGHTER,
                fontSize: styles.FONT_SIZE,
                fontWeight: '500',
                textAlign: 'center'
            }
        ]),
        textResults: StyleSheet.flatten([
            {
                color: styles.COLOR_GREY_LIGHTER,
                fontSize: styles.FONT_SIZE,
                fontWeight: '400',
                textAlign: 'left',
                paddingLeft: 15,
                flex: 3
            }
        ]),



    });

