import React, { Component } from 'react';
import { Actions } from "react-native-router-flux";
import {
    View,
    ScrollView,
    Keyboard
} from 'react-native';

import * as routes from "app/config/sceneKeys";

import CategoryItem from 'app/components/CategoryItem';


import styles from './styles';



export default class CategoriesLayout extends Component {

    constructor() {
        super();

        this.state = {


        }

    }

    componentDidMount() {

    }


    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <View style={style.container}>

                {props.categories.all.map(category =>
                    <CategoryItem
                        key={category.category_id}
                        item={category}
                        onPress={() => this.onPress({
                            title: category.name,
                            categories: category.categories
                        })}
                    />
                )}

            </View>
        )
    }



    onPress = (params) => {

        if (params.categories) {
            Actions.push(routes.SUB_CATEGORIES.key, params);
        } else {
            alert("Сорян, категорій нема)))")
        }

    }
}