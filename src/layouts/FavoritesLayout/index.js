import React from 'react';
import {
    View
} from 'react-native';

import ProductCard from 'app/components/ProductCard';


import styles from './styles';



export default class FavoritesLayout extends React.Component {

    constructor() {
        super();

        this.state = {

            loading: false
        }
    }

    componentDidMount() {

    }


    cards = [
        {
            id: 'sdfqfdq',
            img: 'https://smartmobil.com.ua/files/products/noutbuk-apple-a1534-macbook-12-space-gray-3.800x600w.jpg',
            title: 'Ноутбук MacBook 12" 2015',
            oldPrice: '32 999',
            currency: 'грн',
            price: '25 000',
            rating: '4.5',
            reviews: [
                {
                    id: '1341',
                    rating: '3',
                    title: 'норм',
                    text: 'Какой-то текст'
                },
                {
                    id: 'dsfsdf',
                    rating: '2',
                    title: 'плохо',
                    text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                },
                {
                    id: '1341',
                    rating: '3',
                    title: 'норм',
                    text: 'Какой-то текст'
                },
                {
                    id: 'dsfsdf',
                    rating: '2',
                    title: 'плохо',
                    text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                },
            ],
            status: 'new'
        },
        {
            id: 'ergerg',
            img: 'https://d1ibm7nofgdn3g.cloudfront.net/acer-23099679-5067-large.jpg',
            title: 'Ноутбук Acer ExtensaEX2519-C313234234235 242342352 423423523523532',
            oldPrice: '7 299',
            currency: 'грн',
            price: '6 299',
            rating: '2.3',
            reviews: [
                {
                    id: '1341',
                    rating: '3',
                    title: 'норм',
                    text: 'Какой-то текст'
                },
                {
                    id: 'dsfsdf',
                    rating: '2',
                    title: 'плохо',
                    text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                },
            ],
            status: 'new'
        },

    ]

    render() {
        const { state } = this;

        const style = styles();

        return (
            <View style={style.container}>
                {this.cards.map(card =>
                    <ProductCard
                        key={card.id}
                        card={card}
                        showClose={true}
                    />
                )}
            </View>
        )
    }
}