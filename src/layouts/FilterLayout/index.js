import React from 'react';
import {
    View,
    Text
} from 'react-native';

import TitleOfList from 'app/components/TitleOfList';
import CategoryChecked from 'app/components/CategoryChecked';



import styles from './styles';



export default class FilterLayout extends React.Component {

    constructor() {
        super();

        this.state = {
            cpu: [
                {
                    value: 'AMD_A4',
                    title: 'AMD A4',
                    checked: true
                },
                {
                    value: 'AMD_E',
                    title: 'AMD E +19'
                },
                {
                    value: 'Celeron',
                    title: 'Intel Celeron +100'
                },
                {
                    value: 'Pentium',
                    title: 'Intel Pentium'
                },
            ],
            diagonal: [
                {
                    value: '15-15.6',
                    title: '15-15.6',
                    checked: true
                },
                {
                    value: '17-21',
                    title: '17-21',
                },
            ],
            manufacturer: [
                {
                    value: 'Acer',
                    title: 'Acer',
                    checked: true
                },
                {
                    value: 'Sumsung',
                    title: 'Samsung'
                },
                {
                    value: 'Lenovo',
                    title: 'Lenovo'
                },
                {
                    value: 'Sony',
                    title: 'Sony'
                },
                {
                    value: 'Apple',
                    title: 'Apple'
                },
            ],
            ram: [
                {
                    value: '8',
                    title: '8',
                    checked: true
                },
                {
                    value: '16',
                    title: '16'
                },
                {
                    value: '32',
                    title: '32'
                },
                {
                    value: '54',
                    title: '64'
                },
            ],
            loading: false
        }
    }

    componentWillMount() {

    }


    render() {
        const { state } = this;

        const style = styles();

        return (
            <View>

                <CategoryChecked
                    type='select'
                    title='Процессор'
                    list={state.cpu}
                    callback={(data) => this.select('cpu', data)}
                />


                <CategoryChecked
                    type='select'
                    title='Диагональ'
                    list={state.diagonal}
                    disableBottomBorder={true}
                    callback={(data) => this.select('diagonal', data)}
                />


                <CategoryChecked
                    type='select'
                    title='Производитель'
                    list={state.manufacturer}
                    disableBottomBorder={true}
                    callback={(data) => this.select('manufacturer', data)}
                />

                <CategoryChecked
                    type='select'
                    title='Объем оперативной памяти'
                    list={state.ram}
                    disableBottomBorder={true}
                    callback={(data) => this.select('ram', data)}
                />

            </View>
        )
    }

    select = (name, data) => {
        this.setState({
            [name]: data
        })
    }

    onSubmit = () => {

    }
}