import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {

        ...stylesGlobal.container,
        // paddingVertical: styles.GUTTER
      }

    ]),

    btn: StyleSheet.flatten([
      {
        marginBottom: styles.GUTTER - 10

      }

    ]),

    textContainer: StyleSheet.flatten([
      {
        padding: styles.GUTTER
      }
    ]),

    text: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        fontWeight: '400',
        color: styles.COLOR_BLACK
      }

    ]),

    textBig: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE + 2,
        fontWeight: '700',
        color: styles.COLOR_BLACK,
        marginTop: styles.GUTTER - 5
      }

    ]),

    footer: StyleSheet.flatten([
      {
        padding: styles.GUTTER
      }

    ]),

    footerText: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        fontWeight: '400',
        color: styles.COLOR_BLACK,
        textAlign: 'center',
      }

    ]),

    totalContainer: StyleSheet.flatten([
      {
        backgroundColor: styles.COLOR_GREY_LIGHT,
        padding: styles.GUTTER,
        marginTop: styles.GUTTER
      }

    ]),

    totalTextBlock: StyleSheet.flatten([
      {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: styles.GUTTER,
      }

    ]),

    totalTitle: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE + 2,
        fontWeight: '700',
        color: styles.COLOR_BLACK,
      }

    ]),

    totalText: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        fontWeight: '400',
        color: styles.COLOR_BLACK,
      }

    ]),

    totalResult: StyleSheet.flatten([
      {
        fontWeight: '700',
      }

    ]),


  });

