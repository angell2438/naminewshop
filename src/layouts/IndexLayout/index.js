import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    Keyboard
} from 'react-native';

import ProductCard from 'app/components/ProductCard';
import Slider from 'app/components/Slider';
import Button from 'app/components/Button';






import styles from './styles';



export default class IndexLayout extends Component {

    constructor() {
        super();

        this.state = {


        }

    }


    componentDidMount() {

    }


    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <View>
                <Slider
                    images={props.data.images}
                    style={style.slider}
                />
                <View style={style.container}>
                    <Button
                        text="Каталог товаров"
                        onPress={this.onPressCatalog}
                        style={style.button}
                    />
                    <Text style={style.title}>Новинки</Text>
                </View>
                <View style={style.productsContainer}>
                    {props.data.news.map((card, index) =>
                        <ProductCard
                            key={card.id}
                            index={index + 1}
                            card={card}
                            viewStyle='small'
                        />
                    )}
                </View>
                <View style={style.container}>
                    <Text style={style.title}>Популярные</Text>
                </View>
                <View style={style.productsContainer}>
                    {props.data.news.map((card, index) =>
                        <ProductCard
                            key={card.id}
                            index={index + 1}
                            card={card}
                            viewStyle='small'
                        />
                    )}
                </View>
            </View>
        )
    }

    onPressCatalog = () => {

    }

}