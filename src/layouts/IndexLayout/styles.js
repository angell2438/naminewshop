import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    slider: StyleSheet.flatten([
      {
        paddingBottom: styles.GUTTER
      }

    ]),

    container: StyleSheet.flatten([
      {
        paddingHorizontal: styles.GUTTER
      }

    ]),

    button: StyleSheet.flatten([
      {
        marginBottom: 0
      }

    ]),

    title: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        color: styles.COLOR_BLACK,
        paddingTop: styles.GUTTER,
        paddingBottom: styles.GUTTER - 5,
      }
    ]),

    productsContainer: StyleSheet.flatten([
      {

        justifyContent: 'flex-start',
        flexBasis: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        ...stylesGlobal.borderBottom,
        borderTopWidth: 1,
        borderBottomWidth: 0,
      }

    ]),



  });

