import React, { Component } from 'react';
import {
    ScrollView,

} from 'react-native';

import CategoryItem from 'app/components/CategoryItem';
import TextCustom from 'app/components/TextCustom';


import styles from './styles';



export default class InformationDetailLayout extends Component {

    constructor() {
        super();

        this.state = {


        }

    }

    componentDidMount() {

    }
    // todo Зробити для props.data універсальний текстовий компонент який по type буде корегувати свій вигляд

    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <ScrollView
                bounces={false}
                style={style.container}>

                {props.data.map(item =>
                    <TextCustom
                        key={item.id}
                        type={item.type}
                        text={item.text}
                    />
                )}

            </ScrollView>
        )
    }


    onPress = (params) => {
        const { props } = this;
        // props.getProducts({
        //     category_id: params.category_id
        // });
        // Actions.push(routes.PRODUCTS.key, {...params.title});

    }
}