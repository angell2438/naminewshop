import React, { Component } from 'react';
import {
    ScrollView,
    Keyboard
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import * as routes from "app/config/sceneKeys";

import CategoryItem from 'app/components/CategoryItem';




import styles from './styles';



export default class InformationLayout extends Component {

    constructor() {
        super();

        this.state = {


        }

    }
    

    componentDidMount() {

    }


    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <ScrollView
                bounces={false}
                style={style.container}>

                {props.info.map(item =>
                    <CategoryItem
                        key={item.id}
                        type='subcategory'
                        item={item}
                        onPress={() => this.onPress({
                            id: item.id,
                            title: item.name,
                            data: item.data,
                        })}
                    />
                )}

            </ScrollView>
        )
    }


    onPress = (params) => {
        const { props } = this;
        // props.getProducts({
        //     category_id: params.category_id
        // });
        Actions.push(routes.INFORMATION_DETAIL.key, params);

    }
}