import React, { Component } from 'react';
import {
  Animated,
  View,
  Text
} from 'react-native';
import {
  Actions
} from 'react-native-router-flux';

import styles from './styles';

export default class LoadingLayout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      opacity: new Animated.Value(0)
    };
  }

  componentDidMount() {
    if (this.props.data == 'show') {
      this.show()
    }
  }

  componentWillReceiveProps(np) {
    if (np.data == 'hide') {
      this.close();
    }
  }

  render() {
    const { props, state } = this;
    const style = styles(props);

    return (

      <Animated.View
        style={[
          style.container,
          {
            opacity: state.opacity
          }
        ]}
      >
        <View style={style.overlay} />

        <Text style={style.text}>
          Загрузка...
        </Text>

      </Animated.View>

    )
  }

  show = () => {
    Animated.timing(this.state.opacity, {
      duration: 150,
      toValue: 1
    }).start();
  }

  close = () => {
    Animated.timing(this.state.opacity, {
      duration: 150,
      toValue: 0
    }).start(Actions.pop);
  }
}