import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 5
      },
      props.style && {
        ...props.style
      }
    ]),

    overlay: StyleSheet.flatten([
      {
        backgroundColor: '#fff',
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        opacity: 1
      },
    ]),

    text: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        color: styles.COLOR_RED,
        fontWeight: '500',
        zIndex: 2,
        position: 'relative',
        paddingVertical: styles.GUTTER / 2
      },
    ]),
  });

