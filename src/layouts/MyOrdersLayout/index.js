import React from 'react';
import {
    View, LayoutAnimation, UIManager, Alert
} from 'react-native';
import PropTypes from "prop-types";

import ProductCard from 'app/components/ProductCard';

import styles from './styles';


export default class MyOrdersLayout extends React.Component {


    static propTypes = {
        viewStyle: PropTypes.string,

    };


    constructor() {
        super();
        this.state = {
            loading: false,
            sortedSuccess: [],
        }
    }

    componentDidUpdate() {
        LayoutAnimation.easeInEaseOut();
    }

    componentWillMount() {
        // this.sortByRatingDESC();
    }

    componentDidMount() {
        console.log(this.props);
    }

    cards = [
        {
            id: 'sdfqfdq',
            img: 'https://smartmobil.com.ua/files/products/noutbuk-apple-a1534-macbook-12-space-gray-3.800x600w.jpg',
            title: 'Ноутбук MacBook 12"',
            oldPrice: '32 999',
            currency: 'грн',
            price: '25 000',
            rating: '4.5',
            status: 'new',
            code: '27 775 593',
            date: '24 декабря 2017',
            deliveryStatus: 'self-delivery',
            quantity: 1,
            reviews: [
                {
                    id: '1341',
                    rating: '3',
                    title: 'норм',
                    text: 'Какой-то текст'
                },
                {
                    id: 'dsfsdf',
                    rating: '2',
                    title: 'плохо',
                    text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                },
                {
                    id: '1341',
                    rating: '3',
                    title: 'норм',
                    text: 'Какой-то текст'
                },
                {
                    id: 'dsfsdf',
                    rating: '2',
                    title: 'плохо',
                    text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                },
            ]
        },
        {
            id: 'ergerg',
            img: 'http://www.laptopmag.com/images/wp/laptop-landing/thumb/48355.jpg',
            title: 'Ноутбук Acer ExtensaEX2519-C313234234235 242342352 423423523523532',
            oldPrice: '7 299',
            currency: 'грн',
            price: '6 299',
            rating: '2.3',
            status: 'top',
            code: '157 756 567',
            date: '02 декабря 2018',
            deliveryStatus: 'received',
            quantity: 1,
            reviews: [
                {
                    id: '1341',
                    rating: '3',
                    title: 'норм',
                    text: 'Какой-то текст'
                },
                {
                    id: 'dsfsdf',
                    rating: '2',
                    title: 'плохо',
                    text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                },
            ]
        },
    ]

    render() {
        const { props, state } = this;

        const style = styles();

        return (
            <View style={style.container}>

                {this.cards.map((card, index) =>
                    <ProductCard
                        key={card.id}
                        index={index + 1}
                        card={card}
                        type='shortInfo'
                        viewStyle='standart'
                    />
                )}
            </View>
        )
    }

}
