import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        justifyContent: 'flex-start',
        flexBasis: '100%',
        flexDirection: 'row', 
        flexWrap: 'wrap'
        // ...stylesGlobal.container,
        // paddingTop: styles.GUTTER-5
      }

    ]),


  });

