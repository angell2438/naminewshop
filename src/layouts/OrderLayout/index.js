import React from 'react';
import {
    View,
    Text
} from 'react-native';

import Field from 'app/components/Field';
import TitleOfList from 'app/components/TitleOfList';
import CategoryItem from 'app/components/CategoryItem';
import CategoryChecked from 'app/components/CategoryChecked';
import Button from 'app/components/Button';



import styles from './styles';



export default class OrderLayout extends React.Component {

    constructor() {
        super();

        this.state = {
            fields: {
                name: '',
                phone: '',
                city: '',
                comment: '',
            },
            delivery: [
                {
                    value: 'pickup',
                    title: 'Самовывоз',
                    checked: true
                },
                {
                    value: 'courier',
                    title: 'Курьер'
                },
            ],
            delivery_service: [
                {
                    value: 'pickup',
                    title: 'Новая Почта',
                    checked: true
                },
            ],
            payment_method: [
                {
                    value: 'in_cash',
                    title: 'Наличными',
                    checked: true
                },
                {
                    value: 'non-cash',
                    title: 'Безналичными'
                },
                {
                    value: 'credit',
                    title: 'Кредит'
                },
                {
                    value: 'visa',
                    title: 'Visa/MasterCard'
                },
            ],
            loading: false
        }
    }

    componentWillMount() {

    }


    render() {
        const { state } = this;

        const style = styles();

        const total = [
            {
                id: 'tot1',
                title: 'Сумма',
                result: '6 799 грн'
            },
            {
                id: 'tot2',
                title: 'Доставка',
                result: '50 грн'
            },
            {
                id: 'tot3',
                title: 'Сумма к оплате',
                result: '6 849 грн'
            },
        ]

        return (
            <View>
                <TitleOfList
                    title='Персональные данные'
                />

                <View style={style.container}>
                    <Field
                        placeholder="Город"
                        value={state.fields.city}
                        name="name"
                        icon="arrow_left"
                        callback={this.onChangeInput}
                    />
                    <Field
                        placeholder="Имя и фамилия"
                        value={state.fields.name}
                        name="name"
                        icon="arrow_left"
                        callback={this.onChangeInput}
                    />
                    <Field
                        placeholder="Телефон"
                        value={state.fields.phone}
                        name="phone"
                        icon="arrow_left"
                        callback={this.onChangeInput}
                    />
                </View>

                <TitleOfList
                    title='Способ доставки'
                    marginTop={true}
                />

                <CategoryChecked
                    list={state.delivery}
                    disableBottomBorder={true}
                    callback={(data)=>this.select('delivery', data)}
                />

                <TitleOfList
                    title='Служба доставки'
                />

                <CategoryChecked
                    list={state.delivery_service}
                    disableBottomBorder={true}
                    callback={(data)=>this.select('delivery_service', data)}
                />

                <TitleOfList
                    title='Адрес доставки'
                />

                <CategoryItem
                    type='subcategory'
                    fullWidth={true}
                    item={{
                        name: 'Отделение'
                    }}
                // onPress={}
                />

                <View style={style.textContainer}>
                    <Text style={style.text}>
                        Стоимость доставки
                    </Text>
                    <Text style={style.textBig}>
                        50 грн
                    </Text>
                </View>

                <TitleOfList
                    title='Способ оплаты'
                />

                <CategoryChecked
                    list={state.payment_method}
                    disableBottomBorder={true}
                    callback={(data)=>this.select('payment_method', data)}
                />

                <TitleOfList
                    title='Комментарий к заказу'
                />
                <View style={style.container}>

                    <Field
                        placeholder="Комментарий"
                        value={state.fields.comment}
                        name="text"
                        icon="arrow_left"
                        letterLimit={250}
                        callback={this.onChangeInput}
                    />
                </View>
                <View style={style.totalContainer}>
                    <Text style={style.totalTitle}>
                        Общая сумма заказа:
                    </Text>
                    {total.map(item =>
                        <View
                            key={item.id}
                            style={style.totalTextBlock}
                        >
                            <Text style={style.totalText}>
                                {item.title}:
                            </Text>
                            <Text style={{
                                ...style.totalText,
                                ...style.totalResult
                            }}>
                                {item.result}
                            </Text>
                        </View>
                    )}
                </View>

                <View style={style.footer}>
                    <Button
                        style={style.btn}
                        onPress={this.onSubmit}
                        text='Подтвердить заказ'
                    />
                    <Text style={style.footerText}>
                        Подтверждая заказ, вы соглашаетесь
                        с предоставлением персональных
                        данных интернет-магазина
                    </Text>
                </View>

            </View>
        )
    }

    select = (name, data) => {
        this.setState({
            [name]: data
        })
    }

    onSubmit = () => {

    }

    onChangeInput = (data) => {
        const { state } = this;
        this.setState({
            fields: {
                ...state.fields,
                [data.name]: data.value
            }
        });
    }
}