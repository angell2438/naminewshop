import React from 'react';
import {
    View
} from 'react-native';
import {
    Actions
} from 'react-native-router-flux';

import Password from 'app/components/Password';

import * as routes from "app/config/sceneKeys";




import styles from './styles';



export default class PasswordLayout extends React.Component {

    constructor() {
        super();

        this.state = {
            fields: {
                password: '',
            },
            checked: false,
        }
    }

    componentWillMount() {

    }


    render() {
        const { state } = this;

        const style = styles();


        return (
            <View style={style.container}>
                <Password
                    velue={state.fields.password}
                    checked={state.checked}
                    visiblePassword={this.visiblePassword}
                    onChangeInput={this.onChangeInput}
                />
            </View>
        )
    }

    visiblePassword = () => {
        this.setState({
            checked: !this.state.checked
        })
    }

    onChangeInput = (data) => {
        const { state } = this;
        this.setState({
            field: {
                ...state.field,
                [data.name]: data.value
            }
        });
    }
}