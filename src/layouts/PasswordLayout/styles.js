import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {

        ...stylesGlobal.container,
        paddingTop: 5,
        paddingBottom: styles.GUTTER
      }

    ]),

    bottomLine: StyleSheet.flatten([
      {
        flexDirection: 'row',
        justifyContent: 'space-between',
      }
    ]),


  });

