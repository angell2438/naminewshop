import React from 'react';
import {
    View,
    Text
} from 'react-native';
import PropTypes from "prop-types";

import Content from 'app/components/Content';
import Slider from 'app/components/Slider';
import RatingCustom from 'app/components/RatingCustom';
import ButtonIcon from 'app/components/ButtonIcon';
import CategoryItem from 'app/components/CategoryItem';
import TitleOfList from 'app/components/TitleOfList';
import Review from 'app/components/Review';
import ProductCard from 'app/components/ProductCard';
import Button from 'app/components/Button';


import * as stylesConfig from 'app/config/style';
import styles from './styles';



export default class ProductLayout extends React.Component {


    static propTypes = {
        viewStyle: PropTypes.string,

    };

    // static defaultProps = {
    //     viewStyle: "standart"
    // };

    constructor() {
        super();
        this.state = {
            loading: false
        }
    }

    componentDidMount() {

    }

    render() {
        const { props, state } = this;
        const { product } = props;

        const style = styles(product);



        return (
            <Content
                disableMargin={true}
                showLoading={state.showLoading}
                afterLoading={this.hideLoading}
            >
                <View style={style.wrap}>
                    <View style={style.content}>
                        <View style={style.container}>
                            {/* <Slider
                                images={product.images}
                            /> */}
                            <Text style={style.title}>{product.name}</Text>

                            <View style={style.underTitle}>
                                <Text style={style.smallText}>Код {product.code}</Text>
                                <View style={style.socialInfo}>
                                    <RatingCustom
                                        active={product.rating}
                                        readMode={true}
                                        style={style.rating}
                                    />

                                    <Text style={style.smallText}>
                                        {product.reviews ? product.reviews.length : 0} отзывов
                                    </Text>
                                </View>
                            </View>

                            <View style={style.footer}>
                                <View style={style.pricePart}>
                                    <Text style={style.availability}>
                                        {Number(product.quantity) > 0 && 'Есть в наличии'}
                                        {!Number(product.quantity) < 0 && 'Нет в наличии'}
                                    </Text>
                                    <View style={style.priceDetail}>
                                        <Text style={style.price}>{product.special} {product.currency}</Text>
                                        <Text style={style.oldPrice}>{product.price} {product.currency}</Text>
                                    </View>
                                </View>
                                <ButtonIcon
                                    name='arrow_left'
                                    color={stylesConfig.COLOR_GREY}
                                    onPress={this.share}
                                />
                            </View>

                        </View>


                        <View style={style.descriptionContainer}>
                            <Text style={style.description}>
                                {product.description}
                            </Text>
                        </View>


                        <CategoryItem
                            type='subcategory'
                            fullWidth={true}
                            item={{
                                name: 'Полное описание'
                            }}
                        // onPress={}
                        />

                        <CategoryItem
                            type='subcategory'
                            fullWidth={true}
                            item={{
                                name: 'Доставка, оплата и гарантия'
                            }}
                        // onPress={}
                        />

                        <TitleOfList
                            title='Отзывы'
                        />


                        {/* {product.reviews.map(item =>
                            <Review
                                key={item.id}
                                data={item}
                                showButton={false}
                            />
                        )} */}


                        <CategoryItem
                            type='subcategory'
                            fullWidth={true}
                            item={{
                                name: 'Смотреть все отзывы'
                            }}
                        // onPress={}
                        />

                        <TitleOfList
                            title='Похожие товары'
                        />
                        {/* <View style={style.similarContainer}>
                            {product.similar.map((card, index) =>
                                <ProductCard
                                    key={card.id}
                                    index={index + 1}
                                    card={card}
                                    viewStyle='small'
                                />
                            )}
                        </View> */}
                    </View>
                </View>
            </Content>
        )
    }

    share = () => {
        console.log('share');
    }

    buy = () => {
        console.log('buy')
    }

    hideLoading = () => {
        console.log('hideLoading')
    }
}