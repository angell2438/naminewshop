import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    wrap: StyleSheet.flatten([
      {
        flex: 1
      }

    ]),

    content: StyleSheet.flatten([
      {
        paddingTop: styles.GUTTER,
        paddingBottom: 70
      }

    ]),
    container: StyleSheet.flatten([
      {
        paddingHorizontal: styles.GUTTER,
      }

    ]),

    similarContainer: StyleSheet.flatten([
      {
        justifyContent: 'flex-start',
        flexBasis: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap'
      }

    ]),

    title: StyleSheet.flatten([
      {
        marginTop: styles.MARGIN,
        fontSize: styles.FONT_SIZE,
        fontWeight: "400"
      }
    ]),

    socialInfo: StyleSheet.flatten([
      {
        flexDirection: 'row',
        alignItems: 'center'
      }
    ]),

    rating: StyleSheet.flatten([
      {
        marginRight: styles.GUTTER - 5,
      }
    ]),

    smallText: StyleSheet.flatten([
      {
        color: styles.COLOR_GREY,
        fontSize: styles.FONT_SIZE - 4,
        fontWeight: "400"
      }
    ]),

    footer: StyleSheet.flatten([
      {
        justifyContent: 'space-between',
        flexBasis: '100%',
        flexDirection: 'row',
        paddingVertical: styles.GUTTER - 5,
      }
    ]),

    pricePart: StyleSheet.flatten([
      {

      }
    ]),

    underTitle: StyleSheet.flatten([
      {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        flexBasis: '100%',
        paddingVertical: styles.GUTTER - 5,
        ...stylesGlobal.borderBottom
      }
    ]),

    availability: StyleSheet.flatten([
      {
        fontWeight: '500',
        fontSize: styles.FONT_SIZE - 4,
        color: styles.COLOR_RED
      },
      Number(props.quantity) > 0 && {
        color: styles.COLOR_GREEN
      }
    ]),

    priceDetail: StyleSheet.flatten([
      {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: styles.GUTTER - 5
      }
    ]),

    price: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE + 2,
        color: styles.COLOR_BLACK,
        fontWeight: '700',
      }
    ]),

    oldPrice: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE - 4,
        color: styles.COLOR_GREY,
        fontWeight: '400',
        textDecorationLine: 'line-through',
        marginLeft: styles.GUTTER
      }
    ]),

    descriptionContainer: StyleSheet.flatten([
      {
        paddingVertical: styles.GUTTER - 5,
        paddingHorizontal: styles.GUTTER,
        ...stylesGlobal.borderBottom,
        borderTopWidth: 1,
      }
    ]),

    description: StyleSheet.flatten([
      {
        color: styles.COLOR_BLACK,
        fontSize: styles.FONT_SIZE - 2,
        fontWeight: "400"
      }
    ]),


    actionContainer: StyleSheet.flatten([
      {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: styles.COLOR_GREY_LIGHT,
        padding: styles.GUTTER,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 2,
      }
    ]),

    actionBuy: StyleSheet.flatten([
      {
        marginBottom: 0,
        flexGrow: 2,
        marginLeft: styles.GUTTER,
      }
    ]),

    actionIcon: StyleSheet.flatten([
      {
        flexGrow: 1
      }
    ]),

    actionIconWithBorder: StyleSheet.flatten([
      {
        ...stylesGlobal.borderBottom,
        borderBottomWidth: 0,
        borderLeftWidth: 1
      }
    ]),


  });

