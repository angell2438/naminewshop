import React from 'react';
import {
    View
} from 'react-native';

import Field from 'app/components/Field'
import Button from 'app/components/Button';
import TitleOfList from 'app/components/TitleOfList';
import CategoryChecked from 'app/components/CategoryChecked';



import styles from './styles';



export default class ProfileEditLayout extends React.Component {

    constructor() {
        super();

        this.state = {
            fields: {
                name: '',
                email: '',
                phone: '',
                city: '',
                birthday: '',
            },
            genders: [
                {
                    value: 'woman',
                    title: 'Женский',
                    checked: true
                },
                {
                    value: 'man',
                    title: 'Мужской'
                },
            ],
            loading: false
        }
    }

    componentWillMount() {
        this.fetchUserData();
    }


    render() {
        const { state } = this;

        const style = styles();


        return (
            <View>
                <TitleOfList
                    title='Личные данные'
                />

                <View style={style.container}>
                    <Field
                        placeholder="Имя и фамилия"
                        value={state.fields.name}
                        name="name"
                        icon="arrow_left"
                        callback={this.onChangeInput}
                    />
                    <Field
                        placeholder="Еmail"
                        value={state.fields.email}
                        name="email"
                        icon="arrow_left"
                        callback={this.onChangeInput}
                    />
                    <Field
                        placeholder="Телефон"
                        value={state.fields.phone}
                        name="phone"
                        icon="arrow_left"
                        callback={this.onChangeInput}
                    />
                    <Field
                        placeholder="Город"
                        value={state.fields.city}
                        name="city"
                        icon="arrow_left"
                        callback={this.onChangeInput}
                    />
                    <Field
                        placeholder="Дата рождения"
                        value={state.fields.birthday}
                        name="birthday"
                        icon="arrow_left"
                        callback={this.onChangeInput}
                    />
                </View>

                <TitleOfList
                    title='Пол'
                    marginTop={true}
                />

                <CategoryChecked
                    list={state.genders}
                    callback={this.selectedGender}
                />

                <View style={style.container}>
                    <Button
                        style={style.btn}
                        onPress={this.cancelHandler}
                        text='Сохранить'
                    />

                    <Button
                        style={style.btn}
                        onPress={this.cancelHandler}
                        text='Отмена'
                        isWhite={true}
                    />
                </View>

            </View>
        )
    }

    fetchUserData = (data) => {

        const { state } = this;

        const userData = {
            name: 'Alina Petrova',
            email: 'alina_petrova93@gmail.com',
            phone: '+380 67 607-09-65',
            city: 'Хмельницкий',
            birthday: '08.09.1993',
        }

        const stateFields = { ...state.fields }

        // const userData = [

        //     {
        //         title: 'Пол',
        //         text: 'Женский'
        //     },
        // ]

        for (let key in stateFields) {
            stateFields[key] = userData[key]
        }

        this.setState({
            fields: {
                ...state.fields,
                ...stateFields
            }
        })
    }

    cancelHandler = () => {

    }

    selectedGender = (genders) => {
        this.setState({
            genders
        })
    }

    onChangeInput = (data) => {
        const { state } = this;
        this.setState({
            fields: {
                ...state.fields,
                [data.name]: data.value
            }
        });
    }
}