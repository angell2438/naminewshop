import React from 'react';
import {
    View
} from 'react-native';

import FieldStatic from 'app/components/FieldStatic';
import Button from 'app/components/Button';



import styles from './styles';



export default class ProfileLayout extends React.Component {

    constructor() {
        super();

        this.state = {
            userData: [],
            loading: false
        }
    }

    componentWillMount() {
        this.fetchUserData();
    }

    fetchUserData = (data) => {

        const userData = [
            {
                title: 'Имя и фамилия',
                text: 'Alina Petrova'
            },
            {
                title: 'Email',
                text: 'alina_petrova93@gmail.com'
            },
            {
                title: 'Телефон',
                text: '+380 67 607-09-65'
            },
            {
                title: 'Город',
                text: 'Хмельницкий'
            },
            {
                title: 'Дата рождения',
                text: '08.09.1993'
            },
            {
                title: 'Пол',
                text: 'Женский'
            },
        ]

        this.setState({
            userData
        })
    }

    cancelHandler = () => {

    }

    render() {
        const { state } = this;

        const style = styles();


        return (
            <View style={style.container}>
                {state.userData.map((item, index) =>
                    <FieldStatic
                        key={index+'static'}
                        title={item.title}
                        text={item.text}
                    />
                )}
                <Button
                    style={style.btn}
                    onPress={this.cancelHandler}
                    text='Выход'
                />
            </View>
        )
    }
}