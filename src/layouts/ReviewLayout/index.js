import React, {Component} from 'react';
import {
    View,
    Keyboard
} from 'react-native';

import Field from 'app/components/Field';
import Button from 'app/components/Button';
import RatingCustom from 'app/components/RatingCustom';


import styles from './styles';



export default class ReviewLayout extends Component {

    constructor() {
        super();

        this.state = {
            fields:{
                fullname: '',
                email: '',
                password: '',
            },
          
        }
       
    }

    componentDidMount() {

    }


    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <View style={style.container}>

                <RatingCustom
                    active='2.8'
                    readMode={false}
                    fullView={true}
                    style={style.rating}
                />
              
              <Field
                    placeholder="Имя и фамилия"
                    value={state.fields.fullname}
                    name="fullname"
                    icon="arrow_left"
                    callback={this.onChangeInput}
                />

                <Field
                    placeholder="Email"
                    value={state.fields.email}
                    name="email"
                    icon="arrow_left"
                    callback={this.onChangeInput}
                />

                <Field
                    placeholder="Пароль"
                    value={state.fields.password}
                    name="password"
                    icon="arrow_left"
                    callback={this.onChangeInput}
                />

                 <Button
                    style={style.btn}
                    onPress={this.onSubmit}
                    text='Отправить'
                />
                
            </View>
        )
    }

    onChangeInput = (data) => {
        const { state } = this;
        this.setState({
            field: {
                ...state.field,
                [data.name]: data.value
            }
        });
    }

    onSubmit = () => {
        Keyboard.dismiss();
        
    }
}