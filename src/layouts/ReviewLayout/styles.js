import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        ...stylesGlobal.container,
        paddingTop: styles.GUTTER * 2
      }

    ]),

    rating: StyleSheet.flatten([
      {
        marginBottom: styles.GUTTER,
      },

    ]),

    btn: StyleSheet.flatten([
      {
        marginTop: styles.MARGIN,
        marginBottom: styles.GUTTER * 2
      },

    ]),



  });

