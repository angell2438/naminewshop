import React, { Component } from 'react';
import {
    View
} from 'react-native';
import PropTypes from 'prop-types';

import Content from 'app/components/Content';
import Review from 'app/components/Review';
import styles from './styles';



export default class ReviewsLayout extends Component {

    static propTypes = {
        reviews: PropTypes.array.isRequired,
    }

    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <Content disableMargin={true}>
                <View style={style.container}>
                    {props.reviews.map(item =>
                        <Review
                            key={item.id}
                            data={item}
                        />
                    )}
                </View>
            </Content>
        )
    }

}