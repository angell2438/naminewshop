import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
    TouchableHighlight
} from 'react-native';

import PropTypes from 'prop-types';

import styles from './styles';


export default class Background extends Component {

    static propTypes = {
        viewStyle: PropTypes.string,
    };

    constructor() {
        super();

        this.state = {
            isLoading: false,
        }

    }
    render() {
        const { props, state } = this;
        const style = styles(props);
        return (
            <View style={style.container}>
                <View style={style.logoContainer}>
                    <Image style={style.logo} source={require('app/img/Artboard.png')} />
                </View>
                <Text style={style.boldText}>Найдите</Text>
                <Text style={style.normalText}>интересующие вас товары</Text>
            </View>

        )
    }

}

