import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    ScrollView
} from 'react-native';
import styles from './styles';

import PropTypes from 'prop-types';

import Icon from "app/components/Icon";


import * as stylesConfig from 'app/config/style';

export default class SearchResult extends Component {

    static propTypes = {
        viewStyle: PropTypes.string,
        list: PropTypes.array
    };


    static defaultProps= {
        filtered: []
    };


    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            startArray: [],
            searchFields: ['name']
        }

    }

    componentDidMount() {
        // console.log(this.props);
    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <ScrollView style={style.listContainer}>
                {props.filtered.map((item, i) =>
                    <TouchableOpacity
                        key={i}
                        style={style.listSearch}>
                        <Icon
                            name='search'
                            style={{flex:1}}
                            color={stylesConfig.COLOR_GREY}
                        />
                        <Text style={style.textResults}>{item}</Text>
                        <Icon
                            name='diagonal_arrow'
                            style={{flex:1}}
                            color={stylesConfig.COLOR_GREY}
                        />
                    </TouchableOpacity>
                )}
            </ScrollView>

        )
    }
}

