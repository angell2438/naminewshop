import React, { Component } from 'react';
import {
    Keyboard,
    View,

} from 'react-native';

import PropTypes from 'prop-types';

import { Actions } from 'react-native-router-flux';

import styles from './styles';

import Background from "./Background";
import SearchResult from "./SearchResult";


export default class SearchLayout extends Component {

    static propTypes = {
        viewStyle: PropTypes.string,
        status: PropTypes.bool,

    };

    static defaultProps= {
        status: false
    };

    constructor() {
        super();

        this.state = {
            isLoading: false,
            value: '',
        }

    }

    componentDidMount() {
        console.log(this.props);
        // this.fetchData();
    }

    render() {
        const { props, state } = this;
        const style = styles(props);
        return (
            <View
                style={{paddingVertical: 15, marginTop:0}}
            >
                {props.filtered.length ?  <SearchResult filtered={props.filtered} /> : <Background/>}
            </View>

        )
    }
    onSubmit = () => {
        Keyboard.dismiss();

    }
    onChengInput = () => {

    }

}

