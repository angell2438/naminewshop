import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';


export default props =>
    StyleSheet.create({
        container: StyleSheet.flatten([
            {

                paddingTop: styles.GUTTER * 2,
                marginTop: 10
            }

        ]),

        containerSearchInput: StyleSheet.flatten([
            {
                // ...stylesGlobal.container,
                paddingTop: 0,
                // flexDirection: 'row',
            }

        ]),

        inputMini: StyleSheet.flatten([
            {
                width: '70%',
                marginTop:0
            }
        ]),
        listContainer: StyleSheet.flatten([
            {
                paddingTop: 0,
                paddingHorizontal: 0,
                marginTop: 0,
                width: '100%',
                height: '100%'
            }
        ]),
        listSearch: StyleSheet.flatten([
            {
                height: 36,
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingHorizontal: styles.GUTTER,
                backgroundColor: 'transparent',
                marginBottom: 10,
                ...stylesGlobal.borderBottom
             }
        ]),

        logoContainer: StyleSheet.flatten([
            {
                flexShrink: 0,
                paddingHorizontal: styles.GUTTER,
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
                // height: props.heightLogoContainer,
                marginBottom: styles.GUTTER / 2,
            }
        ]),

        logo: StyleSheet.flatten([
            {
                height: 20
            }
        ]),

        boldText: StyleSheet.flatten([
            {
                color: styles.COLOR_BLACK,
                fontSize: styles.FONT_SIZE * 2,
                fontWeight: '500',
                marginBottom: styles.GUTTER / 2,
                textAlign: 'center'
            }
        ]),

        normalText: StyleSheet.flatten([
            {
                color: styles.COLOR_GREY_LIGHTER,
                fontSize: styles.FONT_SIZE,
                fontWeight: '500',
                textAlign: 'center'
            }
        ]),
        textResults: StyleSheet.flatten([
            {
                color: styles.COLOR_GREY_LIGHTER,
                fontSize: styles.FONT_SIZE,
                fontWeight: '400',
                textAlign: 'left',
                paddingLeft: 15,
                flex: 3
            }
        ]),



    });

