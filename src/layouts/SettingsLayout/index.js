import React from 'react';
import {
    View
} from 'react-native';

import Button from 'app/components/Button';
import TitleOfList from 'app/components/TitleOfList';
import CategoryChecked from 'app/components/CategoryChecked';



import styles from './styles';



export default class SettingsLayout extends React.Component {

    constructor() {
        super();

        this.state = {
            languages: [
                {
                    value: 'russian',
                    title: 'Русский',
                    checked: true
                },
                {
                    value: 'ukrainian',
                    title: 'Українська'
                },
            ],
            loading: false
        }
    }

    componentWillMount() {

    }


    render() {
        const { state } = this;

        const style = styles();


        return (
            <View>
                <TitleOfList
                    title='Пол'
                />

                <CategoryChecked
                    list={state.languages}
                    callback={this.selectedLanguage}
                />

                <View style={style.container}>
                    <Button
                        style={style.btn}
                        onPress={this.cancelHandler}
                        text='Сохранить'
                    />

                    <Button
                        style={style.btn}
                        onPress={this.cancelHandler}
                        text='Отмена'
                        isWhite={true}
                    />
                </View>

            </View>
        )
    }

    cancelHandler = () => {

    }

    selectedLanguage = (language) => {
        this.setState({
            languages
        })
    }


}