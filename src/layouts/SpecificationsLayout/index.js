import React, { Component } from 'react';
import {
    View
} from 'react-native';
import PropTypes from 'prop-types';

import Content from 'app/components/Content';
import Specification from 'app/components/Specification';
import styles from './styles';



export default class SpecificationsLayout extends Component {


    static propTypes = {
        data: PropTypes.array.isRequired,
    }

    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <Content disableMargin={true}>
                <View style={style.container}>
                    {props.data.map(item =>
                        <Specification
                            key={item.id}
                            data={item}
                        />
                    )}
                </View>
            </Content>
        )
    }

}