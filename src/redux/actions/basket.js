import {BASKET, PRODUCTS} from '../types';
import {setOtherSuccess} from "./products";


export const removeSuccess = (id) => ({
  type: BASKET.remove,
  payload: id
})

export const addSuccess = (id) => ({
  type: BASKET.add,
  payload: id
})

export const changeQuantity = (params) => (dispatch) => {
  dispatch({
    type: BASKET.changeQuantity,
    payload: params,

  });

}

export const remove = (id) => (dispatch) => {
  // console.log('id',id);
  dispatch(removeSuccess(id));

}

export const add = (id) => (dispatch) => {
  dispatch(addSuccess(id));
}
