import { PRODUCTS } from '../types';
import api from '../api';

export const getProducts = category_id => dispatch => {
  console.log('category_id ', category_id)
  api.products.get(category_id)
    .then(
      resp => {
        console.log('resp getProducts ', resp)
        dispatch({
          type: PRODUCTS.get,
          category: {
            category_id,
            products: resp.products
          }
        })
      }
    )
    .catch(
      fail => {
        dispatch({
          type: PRODUCTS.get,
          category: fail
        })
      }
    );
}

export const getById = data => dispatch =>

  api.products.getById(data.product_id)
    .then(
      resp => {
        dispatch({
          type: PRODUCTS.getById,
          product: resp.product,
          ...data
        })
      }
    )
    .catch(
      fail => {
        dispatch({
          type: PRODUCTS.get,
          category: fail
        })
      }
    );

export const setOther = params => dispatch =>
  dispatch({
    type: PRODUCTS.setOther,
    other: params
  })

export const getSorted = type => (dispatch, getState) => {
  const { products } = getState().products;
  let sorted = [];
  if (type === PRODUCTS.sortedRaring) {
    sorted = products.sort((a, b) => parseFloat(b.rating) - parseFloat(a.rating));
  } else {
    sorted = products.sort((a, b) => parseFloat(b.price) - parseFloat(a.price));
  }
  dispatch({
    type: PRODUCTS.sortedProducts,
    payload: sorted
  })

}

