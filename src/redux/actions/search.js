import {SEARCH} from '../types';


export const getSearchList = (params) => (dispatch) => {
  dispatch({
    type: SEARCH.listReceive,
    payload: params || []
  });

}



