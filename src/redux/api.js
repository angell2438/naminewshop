import axios from 'axios';

export default {
  products: {

    get: category_id => {
      const imageProp = {
        image_width: "150",
        image_height: "150",
      }

      return axios({
        data: {
          getType: "getProducts",
          ...imageProp,
          category_id,
        }
      })
        .then(res => res.data)
        .catch(() => {
          return Promise.reject({
            'status': false,
            'description': 'Ошибка сервера'
          });
        });
    },

    getById: product_id => {
      const imageProp = {
        image_width: "200",
        image_height: "140",
      }
      // console.log('api data get product ', data)
      return axios({
        data: {
          getType: "getProduct",
          ...imageProp,
          product_id,
        }
      })
        .then(res => res.data)
        .catch(() => {
          return Promise.reject({
            'status': false,
            'description': 'Ошибка сервера'
          });
        });
    }
  },

  categories: {

    get: data => {
      const imageProp = {
        image_width: "32",
        image_height: "32",
      }
      return axios({
        data: {
          getType: "getCategories",
          ...imageProp,
          ...data,
        }
      })
        .then(res => res.data)
        .catch(() => {
          return Promise.reject({
            'status': false,
            'description': 'Ошибка сервера'
          });
        });
    },

    get_by_id: id => {
      const imageProp = {
        image_width: "150",
        image_height: "150",
      }
      return axios({
        data: {
          getType: "getCategories",
          category_id: id,
          ...imageProp,
        }
      })
        .then(res => res.data)
        .catch(() => {
          return Promise.reject({
            'status': false,
            'description': 'Ошибка сервера'
          });
        });
    }
  },

  user: {

    get: data => {

      return axios({
        data: {
          getType: "getUser",
          ...data,
        }
      })
        .then(res => res.data)
        .catch(() => {
          return Promise.reject({
            'status': false,
            'description': 'Ошибка сервера'
          });
        });
    },


  }
}