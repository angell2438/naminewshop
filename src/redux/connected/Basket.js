import {connect} from 'react-redux';
import {remove, add} from 'app/redux/actions/basket';
import Basket from 'app/views/Basket';
import {bindActionCreators} from "redux";

function mapStateToProps(state) {
  return {
    basket: state.basket
  }
}

function mapDispatchToProps(dispatch) {
  return {
    remove: bindActionCreators(remove, dispatch),
    add: bindActionCreators(add, dispatch),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
