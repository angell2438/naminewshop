import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getCategories,
  updateCategories
} from 'app/redux/actions/categories';

import Categories from 'app/views/Categories';


function mapStateToProps(state) {
  return {
    categories: state.categories
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getCategories: bindActionCreators(getCategories, dispatch),
    updateCategories: bindActionCreators(updateCategories, dispatch),

  }
}

export default
  connect(mapStateToProps, mapDispatchToProps)(Categories);
