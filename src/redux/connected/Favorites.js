import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getCategories,
} from 'app/redux/actions/categories';

import Favorites from 'app/views/Favorites';


function mapStateToProps(state) {
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    // getCategories: bindActionCreators(getCategories, dispatch),

  }
}

export default
  connect(mapStateToProps, mapDispatchToProps)(Favorites);
