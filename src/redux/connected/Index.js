import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getProducts,
} from 'app/redux/actions/products';

import Index from 'app/views/Index';


function mapStateToProps(state) {
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getProducts: bindActionCreators(getProducts, dispatch),

  }
}

export default
  connect(mapStateToProps, mapDispatchToProps)(Index);
