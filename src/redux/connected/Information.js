import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getCategories,
  
} from 'app/redux/actions/categories';

import Information from 'app/views/Information';


function mapStateToProps(state) {
  return {
    info: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getCategories: bindActionCreators(getCategories, dispatch)

  }
}

export default
  connect(mapStateToProps, mapDispatchToProps)(Information);
