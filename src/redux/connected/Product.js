import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getById,
} from 'app/redux/actions/products';

import Product from 'app/views/Product';


function mapStateToProps(state) {
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getById: bindActionCreators(getById, dispatch),

  }
}

export default
  connect(mapStateToProps, mapDispatchToProps)(Product);
