import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getProducts,
  getById,
} from 'app/redux/actions/products';

import {add} from 'app/redux/actions/basket';
import {getSorted} from 'app/redux/actions/products';

import Products from 'app/views/Products';


function mapStateToProps(state) {
  return {
    products: state.products,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    add: bindActionCreators(add, dispatch),
    getSorted: bindActionCreators(getSorted, dispatch),
    getProducts: bindActionCreators(getProducts, dispatch),
    getById: bindActionCreators(getById, dispatch),

  }
}

export default
  connect(mapStateToProps, mapDispatchToProps)(Products);
