import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getCategories,
  
} from 'app/redux/actions/categories';

import ProfileEdit from 'app/views/ProfileEdit';


function mapStateToProps(state) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getCategories: bindActionCreators(getCategories, dispatch)

  }
}

export default
  connect(mapStateToProps, mapDispatchToProps)(ProfileEdit);
