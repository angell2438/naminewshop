import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {
  getSearchList,
} from 'app/redux/actions/search';

import SearchLightbox from 'app/views/SearchLightbox';


function mapStateToProps(state) {
  return {
    filtered: state.search.filter,

  }
}

function mapDispatchToProps(dispatch) {
  return {
    getSearchList: bindActionCreators(getSearchList, dispatch),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchLightbox);
