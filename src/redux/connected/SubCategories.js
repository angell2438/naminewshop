import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getProducts,
} from 'app/redux/actions/products';

import SubCategories from 'app/views/SubCategories';


function mapStateToProps(state) {
  return {
    
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getProducts: bindActionCreators(getProducts, dispatch),

  }
}

export default
  connect(mapStateToProps, mapDispatchToProps)(SubCategories);
