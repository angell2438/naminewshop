import {BASKET} from '../types';

const initialState = {
  checkoutArray: [],
}

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case  BASKET.add:

      state.checkoutArray.push(payload)

      return {
        ...state,
        checkoutArray: state.checkoutArray
      }
    case  BASKET.changeQuantity:

      const checkoutArray = state.checkoutArray.map(card => {
        if (card.id === payload.id) {
          card.quantity = payload.quantity;
          card.newPrice = parseInt(`${card.price}`.replace(/ /g, '')) * card.quantity;
        }
        return card
      })

      return {
        ...state,
        checkoutArray: [...checkoutArray]
      }
    case  BASKET.remove:
      console.log('payload', payload);


      // const index = state.checkoutArray.find((item, i) => item.id == id ? i:false);
      // const checkoutArray = state.checkoutArray.splice(index, 1);
      state.checkoutArray.map((item, i) => {
        if (item.id == payload) {
          state.checkoutArray.splice(i, 1)
        }
      })
      return {
        ...state,
        checkoutArray: state.checkoutArray
      }


    default:
      return state
  }
}
