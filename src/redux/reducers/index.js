import { combineReducers } from "redux";
import products from './products';
import categories from './categories';
import search from './search';
import basket from './basket';

export default combineReducers({
    categories,
    products,
    search,
    basket
});