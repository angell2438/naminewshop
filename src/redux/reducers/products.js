import { PRODUCTS } from '../types';

export default function products(
  state = {
    all: [],
    activeId: '',
    activeProduct: {},
    other: {
      // viewStyle: 'big',
      // viewStyle: 'small',
      viewStyle: 'standart'
    },
  },
  action = {}
) {

  switch (action.type) {
    case PRODUCTS.get: {
      let all = [...state.all];
      const product = all.find(item => item.category_id === action.category.category_id);

      if (!product) {
        all.push(action.category)
      }

      return {
        ...state,
        all,
        activeId: action.category.category_id,
      }
    }

    case PRODUCTS.getById: {
      console.log('action product ', action)
      console.log('state ', state)
      let indexCategory = '';
      let indexProduct = '';


      state.all.map((item, iCategory) => {
        console.log('lala vv')
        if (item.category_id == action.category_id) {
          console.log('lala 0')
          item.products.find((product, iProduct) => {
            if (product.product_id == action.product_id) {
              console.log('lala')
              indexProduct = iProduct;
              indexCategory = iCategory;
            }
          })
        }
      })

      console.log('state.all[indexCategory].products[indexProduct]  ', state.all[indexCategory].products[indexProduct] )
      state.all[indexCategory].products[indexProduct] = action.product;
      console.log('state.all[indexCategory].products[indexProduct] 2  ', state.all[indexCategory].products[indexProduct] )

      return {
        ...state,
        activeProduct: action.product
      }
    }

    case PRODUCTS.setOther: {
      return {
        ...state,
        other: {
          ...state.other,
          ...action.other
        }
      }
    }
    case PRODUCTS.sortedProducts: {
      return {
        ...state,
        products: action.payload
      }
    }
    default:
      return state;
  }
}