import {SEARCH} from '../types';

const initialState = {
  filter: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case  SEARCH.listReceive:
      return {
        ...state,
        filter: [...action.payload]
      }
    default:
      return state
  }
}
