export const PRODUCTS = {
  get: 'get_products',
  getById: 'get_by_id',
  setOther: 'set_other',
  sortedRaring: 'sorted_rating',
  sortedPopular: 'sorts_popular',
  sortedProducts: 'sorted_products'
}

export const CATEGORIES = {
  get: 'get_categories',
  get_by_id: 'get_categories_by_id'
}

export const SEARCH = {
  get: 'get_search',
  loaderStart: 'search_loader_start',
  loaderEnd: 'search_loader_end',
  listReceive: 'search_list_receive',
}

export const BASKET = {
  get: 'get_checkout',
  changeQuantity: 'change_quantity',
  remove: 'remove',
  add: 'add'
}

