import { StyleSheet } from "react-native";

import * as styles from "app/config/style";

export default StyleSheet.create({
  container: {
    // flex: 1,
    // alignItems: "stretch",
    paddingHorizontal: styles.GUTTER
  },

  shadow: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: .1,
    elevation: 10, // offset android
  },

  title: {
    color: styles.COLOR_BLACK,
    fontSize: styles.FONT_SIZE+4,
    fontWeight: "500"
  },

  text: {
    color: styles.COLOR_BLACK,
    fontSize: styles.FONT_SIZE,
    fontWeight: "400"
  },
  
  text_middle: {
    color: styles.COLOR_GREY,
    fontSize: styles.FONT_SIZE-2,
    fontWeight: "400"
  },

  text_small: {
    color: styles.COLOR_GREY,
    fontSize: styles.FONT_SIZE-6,
    fontWeight: "400"
  },

  containerScroll: {
    flexGrow: 1
  },

  containerPage: {
    paddingTop: 70,
    flexGrow: 1,
    flex: 1,
  },

  containerScene: {
    flexGrow: 1
  },

  containerRow: {
    paddingLeft: styles.GUTTER,
    paddingRight: styles.GUTTER
  },

  row: {
    marginLeft: -styles.GUTTER / 2,
    marginRight: -styles.GUTTER / 2,
    flexDirection: "row"
  },

  col: {
    paddingLeft: styles.GUTTER / 2,
    paddingRight: styles.GUTTER / 2
  },

  directionRow: {
    flexDirection: "row"
  },

  flex0: {
    flex: 0
  },

  flex1: {
    flex: 1
  },

  selfEnd: {
    alignSelf: "flex-end"
  },

  selfStart: {
    alignSelf: "flex-start"
  },

  selfCenter: {
    alignSelf: "center"
  },

  justifyCenter: {
    justifyContent: "center"
  },

  justifyBetween: {
    justifyContent: "space-between"
  },

  justifyEnd: {
    justifyContent: "flex-end"
  },

  alignStart: {
    alignItems: "flex-start"
  },

  alignCenter: {
    alignItems: "center"
  },

  borderBottom: {
    borderBottomWidth: 1,
    borderColor: styles.COLOR_GREY_MIDDLE
  }
});
