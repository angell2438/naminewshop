import {StyleSheet} from 'react-native';
import theme from './theme';

const styles = StyleSheet.create({
    title: {
        color: theme.colorTxt,
        fontSize: theme.fontSize + 2,
        lineHeight: theme.fontSize + 2,
    },
    sectionHeader: {
        textAlign: 'center',
        marginTop: 30,
        marginBottom: 20,
        fontSize: theme.fontSize + 8,
        lineHeight: theme.fontSize + 8,
        color: theme.colorTxt
    },
    tabStyle: {
        backgroundColor: '#fff',
    },
    tabTextStyle: {
        color: theme.colorTxtDimmed,
    },
    tabActiveTextStyle: {
        color: theme.red,
        fontWeight: '400',
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#e6e6e6',
        fontSize: theme.fontSize,
    },
    txt: {
        color: theme.colorTxt,
        fontSize: theme.fontSize,
        lineHeight: theme.fontSize,
        fontWeight: null
    },
    txtLarge: {
        color: theme.colorTxt,
        fontSize: theme.fontSize + 2,
        lineHeight: theme.fontSize + 2,
    },
    btnTxt: {
        fontSize: theme.fontSize + 2,
        lineHeight: theme.fontSize + 2,
    },
    header: {
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: "#e6e6e6"
    },
    searchBar: {
        backgroundColor: "rgba(0, 0, 0, .05)",
        borderRadius: 5,
        marginBottom: 10
    },
    slide: {
        backgroundColor: '#f1f1f1',
        flex: 1
    },
    image: {
        flex: 1,
        width: '100%',
    },
    listDrawer:{
        borderBottomWidth: 1,
        borderBottomColor: '#e6e6e6',
        marginLeft: 16,
    },
    txtLoader:{
        color: theme.red,
        fontSize: theme.fontSize + 2,
        lineHeight: theme.fontSize + 2,
        textAlign: 'center',
        paddingTop: 20

    },
    footerContainer:{

    },
    navItemStyle:{},
    navSectionStyle:{},
  
    loaderStyle:{
        width:'100%',
        alignItems:'center',
        justifyContent:'center',
        paddingLeft: 15,
        paddingRight: 15,
    },

});

export default styles;
