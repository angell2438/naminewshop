const theme = {
    colorTxt: "#1a1a1a",
    colorTxtDimmed: "#808080",
    red: "#e83c4d",
    green: '#4dc611',
    fontSize: 14,
    backgroundColorDimmed: "#f8f8f8"
};

export default theme