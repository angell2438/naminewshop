import React, { Component } from 'react';

import Content from 'app/components/Content';
import Recovery from 'app/layouts/AuthLayout/Recovery';

class PasswordRecovery extends Component {

    render() {

        return (
            <Content>
                <Recovery />
            </Content>
        );
    }
}

export default PasswordRecovery;