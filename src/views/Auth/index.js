import React, { Component } from 'react';

import Content from 'app/components/Content';
import TabBar from 'app/components/TabBar';
import SignIn from 'app/layouts/AuthLayout/SignIn';
import SignUp from 'app/layouts/AuthLayout/SignUp';


class Auth extends Component {

    render() {
        const tabs = [
            {
                title: 'Вход',
                component: <SignIn />
            },
            {
                title: 'Регистрация',
                component: <SignUp />
            },
        ];

        return (
            <Content>
                <TabBar tabs={tabs} />
            </Content>
        );
    }
}

export default Auth;