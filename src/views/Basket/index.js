import React, {Component} from 'react';

import Content from 'app/components/Content';
import BasketLayout from 'app/layouts/BasketLayout';

import styles from './styles';


export default class Basket extends Component {

  constructor() {
    super();
  }


  render() {
    const {props} = this;
    const style = styles(props);

    return (
      <Content
        style={style.container}
        contentContainerStyle={{flexGrow: 1}}
      >
        <BasketLayout
          checkoutArray={props.basket.checkoutArray}
          remove={props.remove}
        />
      </Content>
    );
  }
}