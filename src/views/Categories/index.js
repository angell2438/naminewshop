import React, { Component } from 'react';

import CategoriesLayout from 'app/layouts/CategoriesLayout';
import Content from 'app/components/Content';

import styles from './styles';


export default class Categories extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        const { props } = this;

        props.getCategories();

    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <CategoriesLayout {...props} />
            </Content>
        );
    }
}