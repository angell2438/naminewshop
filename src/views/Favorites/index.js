import React, { Component } from 'react';
import {
    Container
} from 'native-base';

import FavoritesLayout from 'app/layouts/FavoritesLayout';
import Content from 'app/components/Content';

import styles from './styles';


export default class Favorites extends Component {

    constructor() {
        super();
    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <FavoritesLayout/>
            </Content>
        );
    }
}