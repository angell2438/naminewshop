import React, { Component } from 'react';

import Content from 'app/components/Content';
import FilterLayout from 'app/layouts/FilterLayout';

import styles from './styles';


export default class Filter extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        const { props } = this;

    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <FilterLayout user={props.user} />
            </Content>
        );
    }
}