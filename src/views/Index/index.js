import React, { Component } from 'react';;
import {
    View,

} from 'react-native';

import {
    Actions
} from 'react-native-router-flux';
import Content from 'app/components/Content';
import {
    Text,
    TouchableOpacity,
    Platform,
    StatusBar
} from 'react-native';

import IndexLayout from 'app/layouts/IndexLayout';
import styles from './styles';


export default class Index extends Component {

    static defaultProps = {
        statusBarHeight: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
    };

    state = {
        showBottomIndex: 0
    }

    render() {
        const { state, props } = this;
        const style = styles(props);

        const data = {
            images: [
                'https://smartmobil.com.ua/files/products/noutbuk-apple-a1534-macbook-12-space-gray-3.800x600w.jpg',
                'http://www.laptopmag.com/images/wp/laptop-landing/thumb/48355.jpg',
                'http://notebook.lviv.ua/images/44/noutbuk-asus-x541ua-85683461237284_small6.jpg',
            ],
            news: [
                {
                    id: 'sdfqfdq',
                    img: 'https://smartmobil.com.ua/files/products/noutbuk-apple-a1534-macbook-12-space-gray-3.800x600w.jpg',
                    title: 'Ноутбук MacBook 12"',
                    oldPrice: '32 999',
                    currency: 'грн',
                    price: '25 000',
                    rating: '4.5',
                    status: 'new',
                    reviews: [
                        {
                            id: '1341',
                            rating: '3',
                            title: 'норм',
                            text: 'Какой-то текст'
                        },
                        {
                            id: 'dsfsdf',
                            rating: '2',
                            title: 'плохо',
                            text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                        },
                        {
                            id: '1341',
                            rating: '3',
                            title: 'норм',
                            text: 'Какой-то текст'
                        },
                        {
                            id: 'dsfsdf',
                            rating: '2',
                            title: 'плохо',
                            text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                        },
                    ]
                },
                {
                    id: 'ergerg',
                    img: 'http://www.laptopmag.com/images/wp/laptop-landing/thumb/48355.jpg',
                    title: 'Ноутбук Acer ExtensaEX2519-C313234234235 242342352 423423523523532',
                    oldPrice: '7 299',
                    currency: 'грн',
                    price: '6 299',
                    rating: '2.3',
                    status: 'top',
                    reviews: [
                        {
                            id: '1341',
                            rating: '3',
                            title: 'норм',
                            text: 'Какой-то текст'
                        },
                        {
                            id: 'dsfsdf',
                            rating: '2',
                            title: 'плохо',
                            text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                        },
                    ]
                },
            ]
        }

        return (
            <Content style={style.container}>
                <IndexLayout data={data}/>
            </Content>
        );
    }
}