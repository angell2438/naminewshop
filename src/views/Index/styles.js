import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        // marginTop: 160
        // ...stylesGlobal.container,
        // paddingTop: styles.GUTTER * 2
        marginTop: styles.NAVBAR_WITHOUT_FILTER,
        flex: 1
      }

    ]),

  });

