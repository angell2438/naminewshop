import React, { Component } from 'react';

import Content from 'app/components/Content';
import InformationLayout from 'app/layouts/InformationLayout';

import styles from './styles';


export default class Information extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        const { props } = this;

    }

    info = [
        {
            id: 'in1',
            name: 'O магазине',
            data: []      
        },
        {
            id: 'in2',
            name: 'Доставка и оплата',
            data: [
                {
                    id: 'ind1',
                    type: 'description',
                    text: `Внимание! Заказывая доставку товара сторонними курьерскими службами (Нова пошта, Мист Экспресс ) - при получении обязательно проверяйте наличие всего товара в заказе, а так же его внешний вид. В случае повреждения товара, или неполной комплектации заказа - необходимо отказаться от получения товара и его оплаты, а так же составить акт(претензию) . В случае если данная ситуация будет иметь место - обязательно сообщите нам о случившемся по тел: (044) 537-02-22, (044) 503-80-80.`,
                },
                {
                    id: 'ind2',
                    type: 'title',
                    text: 'Способы доставки'
                },
                {
                    id: 'ind3',
                    type: 'title_small',
                    text: '«Нова пошта»'
                },
                {
                    id: 'ind4',
                    type: 'text',
                    text: `Доставка в отделение «Нова пошта». С помощью доставки «Нова пошта», Вы можете получить товар даже в самых отдаленных уголках Украины.`
                },
                {
                    id: 'ind5',
                    type: 'title_small',
                    text: 'Адресная доставка по Киеву и области'
                },
                {
                    id: 'ind6',
                    type: 'text',
                    text: `Доставка курьером по адресу. Доставка производится до входа в здание. Стоимость доставки 45 грн.`
                },
                {
                    id: 'ind7',
                    type: 'title_small',
                    text: 'Мист Экспресс'
                },
                {
                    id: 'ind8',
                    type: 'text',
                    text: `Доставка курьером на дом.`
                },
            ]   
        },
        {
            id: 'in3',
            name: 'Гарантия',
            data: []
        },
        {
            id: 'in4',
            name: 'Сотрудничество с нами',
            data: []
        },
    ]

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <InformationLayout info={this.info} />
            </Content>
        );
    }
}