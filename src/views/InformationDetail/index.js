import React, { Component } from 'react';
import { Actions } from "react-native-router-flux";
import InformationDetailLayout  from 'app/layouts/InformationDetailLayout';
import Content from 'app/components/Content';

import styles from './styles';


export default class InformationDetail extends Component {

    constructor() {
        super();
    }

    componentWillMount(){
        const {props} = this;

    }

    render() {
        const { props } = this;
        const style = styles(props);
        
        return (
            <Content>
                <InformationDetailLayout {...props}/>
            </Content>
        );
    }
}