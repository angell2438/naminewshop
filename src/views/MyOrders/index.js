import React, { Component } from 'react';;

import {
    Actions
} from 'react-native-router-flux';

import MyOrdersLayout from 'app/layouts/MyOrdersLayout';
import Content from 'app/components/Content';

import styles from './styles';


export default class Products extends Component {

    constructor() {
        super();
    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <MyOrdersLayout/>
            </Content>
        );
    }
}