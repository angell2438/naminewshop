import React, { Component } from 'react';

import Content from 'app/components/Content';
import OrderLayout from 'app/layouts/OrderLayout';

import styles from './styles';


export default class Order extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        const { props } = this;

    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <OrderLayout user={props.user} />
            </Content>
        );
    }
}