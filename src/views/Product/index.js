import React, { Component } from 'react';;
import {
    View,
    Text,
    TouchableOpacity,
    Platform,
    StatusBar
} from 'react-native';
import {
    Actions
} from 'react-native-router-flux';

import TabBar from 'app/components/TabBar';
import ProductLayout from 'app/layouts/ProductLayout';
import SpecificationsLayout from 'app/layouts/SpecificationsLayout';
import ReviewsLayout from 'app/layouts/ReviewsLayout';
import Icon from 'app/components/Icon';
import ButtonIcon from 'app/components/ButtonIcon';
import Button from 'app/components/Button';

import * as routes from "app/config/sceneKeys";

import stylesConfig from 'app/themes/global';
import styles from './styles';


export default class Product extends Component {

    static defaultProps = {
        statusBarHeight: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
    };

    state = {
        showBottomIndex: 0
    }

  

    componentDidMount() {
        const { props } = this;
        const {
            product_id,
            category_id
        } = props;
        // props.getById({ product_id, category_id }).then(() => {
        //     Actions.loading('hide');
        // });
    }

    componentWillReceiveProps(np) {
   

    }

    render() {
        const { state, props } = this;
        const style = styles(props);

        const data = {
            title: 'Ноутбук Acer Extensa EX2519-C313 (NX.EFAEU.054) Black',
            images: [
                'https://smartmobil.com.ua/files/products/noutbuk-apple-a1534-macbook-12-space-gray-3.800x600w.jpg',
                'http://www.laptopmag.com/images/wp/laptop-landing/thumb/48355.jpg',
                'http://notebook.lviv.ua/images/44/noutbuk-asus-x541ua-85683461237284_small6.jpg',
            ],
            code: '27 775 593',
            oldPrice: '32 999',
            currency: 'грн',
            price: '25 000',
            rating: '2.5',
            availability: true,
            description: 'Экран 15.6` (1366x768) HD LED, матовый / Intel Celeron N3060 (1.6 - 2.48 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics 400 / Без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / без ОС / 2.4 кг / черный',
            specifications: [
                {
                    id: 'sp1',
                    title: 'Процессор',
                    detail: 'Двухъядерный Intel Celeron N3060 (1.6 - 2.48 ГГц)'
                },
                {
                    id: 'sp2',
                    title: 'Диагональ экрана',
                    detail: '15.6" (1366x768) WXGA HD'
                },
                {
                    id: 'sp3',
                    title: 'Объем оперативной памяти',
                    detail: '4 ГБ'
                },
                {
                    id: 'sp4',
                    title: 'Краткие характеристики',
                    detail: "Экран 15.6'' (1366x768) HD LED, матовый / Intel Celeron N3060 (1.6 - 2.48 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics 400 / Без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / без ОС / 2.4 кг / черный"
                },
                {
                    id: 'sp5',
                    title: 'Объём накопителя',
                    detail: '500 ГБ'
                },
                {
                    id: 'sp6',
                    title: 'Оптический привод',
                    detail: 'Отсутствует'
                },
                {
                    id: 'sp7',
                    title: 'Цвет',
                    detail: 'Черный'
                },
                {
                    id: 'sp8',
                    title: 'Клавиатура',
                    detail: 'Без подсветки'
                },
                {
                    id: 'sp9',
                    title: 'Батарея',
                    detail: 'Несъемная'
                },
                {
                    id: 'sp10',
                    title: 'Украинская раскладка клавиатуры',
                    detail: 'C'
                },
                {
                    id: 'sp11',
                    title: 'Количество слотов для оперативной памяти',
                    detail: '1'
                },
                {
                    id: 'sp12',
                    title: 'Вес',
                    detail: '2.4 кг'
                },
                {
                    id: 'sp13',
                    title: 'Тип оперативной памяти',
                    detail: 'DDR3'
                },
                {
                    id: 'sp14',
                    title: 'Дополнительные возможности',
                    detail: 'Веб-камера Встроенный микрофон Стереодинамики'
                },
            ],
            // fullDescription: 'FULL DESC))) Экран 15.6` (1366x768) HD LED, матовый / Intel Celeron N3060 (1.6 - 2.48 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics 400 / Без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / без ОС / 2.4 кг / черный',
            reviews: [
                {
                    id: 'rew1',
                    name: 'lala',
                    date: '02.10.2019',
                    text: 'lalalaldsfdsfsdfdsfdsfds lalalaldsfdsfsdfdsfdsfds lalalaldsfdsfsdfdsfdsfds lalalaldsfdsfsdfdsfdsfds lalalaldsfdsfsdfdsfdsfds lalalaldsfdsfsdfdsfdsfds lalalaldsfdsfsdfdsfdsfds lalalaldsfdsfsdfdsfdsfds dsf dsf dsfdsfdsf dsfdsf dsf '
                }
            ],
            similar: [
                {
                    id: 'sdfqfdq',
                    image: 'https://smartmobil.com.ua/files/products/noutbuk-apple-a1534-macbook-12-space-gray-3.800x600w.jpg',
                    title: 'Ноутбук MacBook 12"',
                    oldPrice: '32 999',
                    currency: 'грн',
                    price: '25 000',
                    rating: '4.5',
                    status: 'new',
                    reviews: [
                        {
                            id: '1341',
                            rating: '3',
                            title: 'норм',
                            text: 'Какой-то текст'
                        },
                        {
                            id: 'dsfsdf',
                            rating: '2',
                            title: 'плохо',
                            text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                        },
                        {
                            id: '1341',
                            rating: '3',
                            title: 'норм',
                            text: 'Какой-то текст'
                        },
                        {
                            id: 'dsfsdf',
                            rating: '2',
                            title: 'плохо',
                            text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                        },
                    ]
                },
                {
                    id: 'ergerg',
                    image: 'https://smartmobil.com.ua/files/products/noutbuk-apple-a1534-macbook-12-space-gray-3.800x600w.jpg',
                    title: 'Ноутбук Acer ExtensaEX2519-C313234234235 242342352 423423523523532',
                    oldPrice: '7 299',
                    currency: 'грн',
                    price: '6 299',
                    rating: '2.3',
                    status: 'top',
                    reviews: [
                        {
                            id: '1341',
                            rating: '3',
                            title: 'норм',
                            text: 'Какой-то текст'
                        },
                        {
                            id: 'dsfsdf',
                            rating: '2',
                            title: 'плохо',
                            text: 'Какой-то текст Какой-то текст Какой-то текст Какой-то текст'
                        },
                    ]
                },
            ]
        }

        const tabs = [
            {
                title: 'О товаре',
                component: <ProductLayout product={props.products.activeProduct} />,
                bottomPart:
                    <View style={style.actionContainer}>
                        <ButtonIcon
                            name='arrow_left'
                            color={stylesConfig.COLOR_GREY}
                            onPress={this.share}
                            style={style.actionIcon}
                        />
                        <ButtonIcon
                            name='arrow_left'
                            color={stylesConfig.COLOR_GREY}
                            onPress={this.share}
                            style={{
                                ...style.actionIcon,
                                ...style.actionIconWithBorder
                            }}
                        />
                        <Button
                            text="Купить сейчас"
                            onPress={this.buy}
                            styleType='big'
                            style={style.actionBuy}
                        />
                    </View>
            },
            {
                title: 'Характеристики',
                component: <SpecificationsLayout data={data.specifications} />
            },
            {
                title: 'Отзывы',
                component: <ReviewsLayout reviews={data.reviews} />,
                bottomPart:
                    <TouchableOpacity
                        onPress={this.newReview}
                        style={style.btnWithIcon}
                        activeOpacity={.8}
                    >
                        <Icon
                            name='arrow_left'
                            style={style.btnWithIcon_Icon}
                        />
                        <Text style={style.btnWithIcon_Text}>
                            Написать отзыв
                        </Text>
                    </TouchableOpacity>
            }
        ];


        return (
            <View style={style.container}>
                <TabBar
                    tabs={tabs}
                    cb={this.changeBottomPart}
                />
                {tabs[state.showBottomIndex].bottomPart}
            </View>
        );
    }

    changeBottomPart = (data) => {
        this.setState({
            showBottomIndex: data.i
        })
    }

    buy = () => {

    }

    newReview = () => {
        Actions[routes.REVIEW.key].call();

    }
}