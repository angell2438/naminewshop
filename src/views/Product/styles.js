import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        // marginTop: 160
        // ...stylesGlobal.container,
        // paddingTop: styles.GUTTER * 2
        marginTop: props.statusBarHeight + 44,
        flex: 1
      }

    ]),

    actionContainer: StyleSheet.flatten([
      {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: styles.COLOR_GREY_LIGHT,
        padding: styles.GUTTER,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 2,
      }
    ]),

    actionBuy: StyleSheet.flatten([
      {
        marginBottom: 0,
        flexGrow: 2,
        marginLeft: styles.GUTTER,
      }
    ]),

    actionIcon: StyleSheet.flatten([
      {
        flexGrow: 1
      }
    ]),

    actionIconWithBorder: StyleSheet.flatten([
      {
        ...stylesGlobal.borderBottom,
        borderBottomWidth: 0,
        borderLeftWidth: 1
      }
    ]),

    btnWithIcon: StyleSheet.flatten([
      {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: styles.COLOR_GREY_LIGHT,
        height: 70,
      }
    ]),

    btnWithIcon_Icon: StyleSheet.flatten([
      {

      }
    ]),

    btnWithIcon_Text: StyleSheet.flatten([
      {
        fontSize: styles.FONT_SIZE,
        color: styles.COLOR_BLACK,
        fontWeight: '500',
        paddingLeft: styles.GUTTER
      }
    ]),





  });

