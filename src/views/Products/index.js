import React, { Component } from 'react';;

import {
    Actions
} from 'react-native-router-flux';

import {
    View,
    LayoutAnimation,
    UIManager,
    Alert
} from 'react-native';

import Content from 'app/components/Content';

import PropTypes from "prop-types";

import ProductCard from 'app/components/ProductCard';

import * as routes from "app/config/sceneKeys";

import styles from './styles';


UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

export default class Products extends Component {


    static propTypes = {
        title: PropTypes.string,
        viewStyle: PropTypes.string,
        categoryId: PropTypes.string,
        products: PropTypes.object,
        viewStyle: PropTypes.string,

    };

    constructor() {
        super();

        this.state = {
            products: [],
            showLoading: false,
            sortedSuccess: [],
        }
    }

    componentDidUpdate() {
        LayoutAnimation.easeInEaseOut();
    }

    componentDidMount() {
        const { props } = this;

        props.getProducts(props.category_id);
        this.toggleLoading();

    }

    componentWillReceiveProps(np) {
        this.setProducts(np)
    }

    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <Content
                style={style.container}
                showLoading={state.showLoading}
            >
                {state.products.map((product, index) =>
                    <ProductCard
                        key={product.product_id}
                        index={index + 1}
                        card={product}
                        viewStyle={props.products.other.viewStyle}
                        onPress={this.onPressItem}
                        onAdd={(e) => this.onAdd(e, product)}
                    />
                )}
            </Content>
        );
    }

    setProducts = (props) => {
        const category = props.products.all.find(item => item.category_id == props.category_id);

        if (category) {
            this.setState({
                products: category.products
            }, () => this.toggleLoading())
        }

    }

    onPressItem = (product_id) => {
        const { props } = this;
        console.log('props click  ', props)
        this.setState({
            showLoading: true
        }, () => props.getById({
            product_id,
            category_id: props.category_id
        }).then(() => {
            this.setState({
                showLoading: false
            });
            Actions.push(routes.PRODUCT.key, {
                title: props.title
            });
        }));




    }

    onAdd = (e, product) => {
        e.stopPropagation();

        const { props } = this;
        console.log('card', product);
        props.add(product)
        Alert.alert('Товар: ' + product.title + 'добавлен в корзизну');

    }

    toggleLoading = () => {
        const { state } = this;

        this.setState({
            showLoading: !state.showLoading
        })
    }
}