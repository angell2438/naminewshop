import { StyleSheet } from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        marginTop: styles.NAVBAR_WITH_FILTER
        // ...stylesGlobal.container,
        // paddingTop: styles.GUTTER * 2
      }

    ]),

    title: StyleSheet.flatten([
      {
        ...stylesGlobal.title,
        textAlign: 'center',
        marginBottom: styles.GUTTER / 2
      },

    ]),




  });

