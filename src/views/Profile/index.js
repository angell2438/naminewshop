import React, { Component } from 'react';

import TabBar from 'app/components/TabBar';
import Content from 'app/components/Content';
import ProfileLayout from 'app/layouts/ProfileLayout';
import PasswordLayout from 'app/layouts/PasswordLayout';

import styles from './styles';


export default class Profile extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        const { props } = this;

    }

    render() {
        const { props } = this;
        const style = styles(props);

        const tabs = [
            {
                title: 'Личные данные',
                component: <ProfileLayout user={props.user} />
            },
            {
                title: 'Пароль',
                component: <PasswordLayout user={props.user} />
            },
        ];

        return (
            <Content>
                <TabBar tabs={tabs} />
            </Content>
        );
    }
}