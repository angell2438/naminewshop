import React, { Component } from 'react';

import Content from 'app/components/Content';
import ProfileEditLayout from 'app/layouts/ProfileEditLayout';

import styles from './styles';


export default class ProfileEdit extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        const { props } = this;

    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <ProfileEditLayout user={props.user} />
            </Content>
        );
    }
}