import React, { Component } from 'react';

import ReviewLayout  from 'app/layouts/ReviewLayout';
import Content from 'app/components/Content';

import styles from './styles';


export default class Review extends Component {

    constructor() {
        super();
    }

    componentDidMount(){
        const {props} = this;

    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <ReviewLayout/>
            </Content>
        );
    }
}