import React, {Component} from 'react';

import SearchLayout from 'app/layouts/SearchLayout';
import Content from 'app/components/Content';

import styles from './styles';

export default class SearchLightbox extends Component {

  constructor() {
    super();
  }

  componentDidMount() {
    const {props} = this;

  }


  render() {
    const {props} = this;
    const style = styles(props);

    return (
      <Content>
        <SearchLayout
          isLoading={props.isLoading}
          filtered={props.filtered}
          searchArray={props.searchArray}

        />
      </Content>
    );
  }
}