import {StyleSheet} from 'react-native';
import * as styles from 'app/config/style';
import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        // ...stylesGlobal.container,
        paddingTop: 0,
        marginTop: 0,
      }

    ]),

    title: StyleSheet.flatten([
      {
        ...stylesGlobal.title,
        textAlign: 'center',
        marginBottom: styles.GUTTER / 2
      },

    ]),

  });

