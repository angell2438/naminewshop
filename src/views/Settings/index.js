import React, { Component } from 'react';

import Content from 'app/components/Content';
import SettingsLayout from 'app/layouts/SettingsLayout';

import styles from './styles';


export default class Settings extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        const { props } = this;

    }

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <Content>
                <SettingsLayout user={props.user} />
            </Content>
        );
    }
}