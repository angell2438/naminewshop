import React, { Component } from 'react';
import { Actions } from "react-native-router-flux";
import Content from 'app/components/Content';

import * as routes from "app/config/sceneKeys";

import CategoryItem from 'app/components/CategoryItem';

import styles from './styles';


export default class SubCategories extends Component {

    constructor() {
        super();

        this.state = {
            showLoading: false
        }
    }

    // componentDidMount() {
    //     setTimeout(() => {
    //         this.setState({
    //             showLoading: false
    //         })
    //     }, 3000)
    // }

    render() {
        const { props, state } = this;
        const style = styles(props);

        return (
            <Content
                showLoading={state.showLoading}
                afterLoading={this.hideLoading}
                style={style.container}
            >

                {props.categories.map(category =>
                    <CategoryItem
                        key={category.category_id}
                        type='subcategory'
                        item={category}
                        onPress={() => this.onPress({
                            category_id: category.category_id,
                            title: category.name,
                        })}
                    />
                )}

            </Content>
        );
    }

    onPress = (params) => {
        const { props } = this;
        // props.getProducts({
        //     category_id: params.category_id
        // });

        // this.setState({
        //     showLoading: true
        // })

        Actions.push(routes.PRODUCTS.key, { ...params });
    }

    hideLoading = () => {
        console.log('hide.loading')
    }
}